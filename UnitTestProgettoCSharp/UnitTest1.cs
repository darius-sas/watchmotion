﻿using System;
using ProgettoCSharp.DataManagement;
using ProgettoCSharp.DataAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Security.Permissions;
using System.Windows.Forms;

namespace UnitTestProgettoCSharp
{
	[TestClass]
	public class UnitTest1
	{
		const int TEST_PORT = 45555;
		const int TEST_FILE_LENGTH = 51992; // Acquisizioni Francesco 2, il SECONDO file.
		string TEST_LOG_FILE_PATH = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
		string testFileName = "";
		float[] array = { 6.577866f, 9.161461f, 9.259945f, 9.809061f, 11.28543f, 11.83053f, 11.98623f, 13.10919f, 6.566203f, 6.33541f, 6.212524f, 5.028143f, 4.174668f, 6.355602f, 7.315829f, 7.315629f, 6.985676f, 7.108304f, 7.188203f, 7.15626f, 6.985293f, 6.998065f, 6.885552f, 6.796203f, 7.000008f, 7.539501f, 7.588821f, 7.460365f, 7.467453f, 7.683998f, 7.72382f, 7.912745f, 8.112736f, 8.082547f, 7.925981f, 7.949388f, 8.098762f, 8.5004f, 8.718492f, 8.876859f, 8.979915f, 8.639924f, 9.177109f, 10.20458f, 10.41396f, 10.45009f, 12.32062f, 13.15196f, 12.50391f, 10.17625f, 10.80544f };
		Director dir;
		FileIOPermission permissions;

		public UnitTest1()
		{
			dir = new Director(TEST_PORT);
			// Acquista i permessi per scrivere sul Desktop.
			permissions = new FileIOPermission(FileIOPermissionAccess.AllAccess, TEST_LOG_FILE_PATH);
			permissions.Assert();
			// Codice per usare il . al posto della , nei decimali

		}

		[TestMethod]
		public void TestFunzioalitaBase()
		{
			System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
			customCulture.NumberFormat.NumberDecimalSeparator = ",";
			Thread.CurrentThread.CurrentCulture = customCulture;
			float stdDev = Analysis.StdDev(array);
			float mean = Analysis.Mean(array);
			int k = 5;
			float[] smooting = Analysis.Smoothing(array, 5);
			float[] expsmooting = Analysis.ExpSmoothing(array, Thresholds.EXP_SMOOTH_ALPHA_MOTION);
			float[] fstd = Analysis.FloatingStdDev(array, k);
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("Media:; " + mean+";"); sb.Append("DevStd:; " + stdDev+"\n");
			sb.Append("Dati;Smoothing;Exp Smoothing;DevStd Mobile\n");
			for (int i = 0; i < array.Length; i++)
			{
				sb.Append(array[i] + ";");
				sb.Append(smooting[i] + ";");
				sb.Append(expsmooting[i] + ";");
				sb.Append(fstd[i] + ";");
				sb.Append(Environment.NewLine);
			}
			System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop)+"\\testAnalysis.csv", sb.ToString());
		}
	
		[TestMethod]
		public void TestDerivata()
		{
			float[] derivata = Analysis.RIfunc(array, 1);
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("Dati;Rapp. Inc.\n"); 
			for (int i = 0; i < array.Length; i++)
			{
				sb.Append(array[i]+";");
				sb.Append(derivata[i]);
				sb.Append(Environment.NewLine);
			}
			System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\RIfunctest.csv", sb.ToString());
		}
	}
}
