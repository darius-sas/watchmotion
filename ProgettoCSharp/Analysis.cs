﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ProgettoCSharp.DataManagement;
using System.IO;
namespace ProgettoCSharp
{
	namespace DataAnalysis
	{
        public enum PositionState
        {
            LAY,
            STAND,
            SIT,
            LAYSIT
        };
		public static class Thresholds
		{
			private static float min_std = 0.8f;
			private static float min_degrees = 10f;
			private static float alpha_motion = 0.1f;
			private static float alpha_laystandsit = 0.2f;
			private static float discontinuity = 1.45f;
			/// <summary>
			/// Deviazione standard minima per confermare il movimento.
			/// </summary>
			public static float MIN_STDDEV_MOTION { get { return min_std; } set { min_std = value; } }
			/// <summary>
			/// Gradi minimi per confermare la girata.
			/// </summary>
			public static float MIN_DEGREES_MOTION { get { return min_degrees; } set { min_degrees = value; } }
			/// <summary>
			/// Cefficiente di smoothing alpha consigliato per il moto.
			/// </summary>
			public static float EXP_SMOOTH_ALPHA_MOTION { get { return alpha_motion; } set { alpha_motion = value; } }
			/// <summary>
			/// Soglia massima per la deviazione standard negli algoritmi per LayStandSit
			/// </summary>
			public static float ALPHA_LAYSTANDSIT { get { return alpha_laystandsit; } set { alpha_laystandsit = value; } }
			public static float DISCONTINUITY { get { return discontinuity; } set { discontinuity = value; } }
			public const int APPROX_WINDOW_SIZE = 7;
		}

		public delegate float Dynamic(float[] a, int start, int end);
		public static class Analysis
		{
			public static float sign = 0f;
			/// <summary>
			/// funzione che calcola l'intensita' di un vettore di 3 elementi
			/// </summary>
			/// <param name="campione1">primo campione</param>
			/// <param name="campione2">secondo campione</param>
			/// <param name="campione3">terzo campione</param>
			/// <returns>intensita di tipo float del vettore</returns>
			public static float Module(float campione1, float campione2, float campione3)
			{
				double moddac = 0d;
				moddac = Math.Pow(campione1, 2) +
					Math.Pow(campione2, 2) +
					Math.Pow(campione3, 2);
				return (float)Math.Sqrt(moddac);
			}

			/// <summary>
			/// I funzionalita di base:
			/// in ingresso un vettore di tre dimensioni samplewin[9][N][S] 
			/// </summary>
			/// <param name="campioni">vettore due dimensioni contenente tool e campioni di samplewindow</param>
			/// <param name="tool">selezione dei tool su cui fare il modulo:
			/// - 1: accelerometro
			/// - 2: giroscopio
			/// - 3: magnetometro</param>
			/// <param name="sensor">selezione del sensore su cui fare il modulo</param>
			/// <returns>array float contenente il modulo dei campioni</returns>
			public static float[] Module(float[, ,] campioni, int tool, int sensor)
			{
				float[] moddac = new float[campioni.GetLength(1)];
				switch (tool)
				{
					case 1: tool = 0; break;
					case 2: tool = 3; break;
					case 3: tool = 6; break;
					default:
						throw new IndexOutOfRangeException("Il parametro tool ha un valore non accettabile.");
				}
				for (int i = 0; i < campioni.Length; i++)
				{
					moddac[i] = Module(campioni[tool, i, sensor], campioni[tool + 1, i, sensor], campioni[tool + 2, i, sensor]);
				}
				return moddac;
			}

			/// <summary>
			/// Esegue lo smoothing dell'array in input.
			/// </summary>
			/// <param name="a">Array di cui eseguire lo smoothing</param>
			/// <param name="window">Grado approssimazione. Un valore maggiore indica una precisione maggiore.</param>
			/// <returns>L'array dopo lo smoothing.</returns>
			public static float[] Smoothing(float[] a, int window)
			{
				return OperationExecution(a, window, Mean);
			}

			/// <summary>
			/// Esegue lo smoothing esponenziale dell'array.
			/// </summary>
			/// <param name="a">Array del quale effettuare lo smooothing.</param>
			/// <param name="alpha">Coefficiente di smorzamento, compreso tra 0 e 1.</param>
			/// <returns></returns>
			public static float[] ExpSmoothing(float[] a, float alpha )
			{
				if (alpha < 0 || alpha > 1)
				{
					throw new ArgumentOutOfRangeException("Il coefficiente alpha deve avere un valore compreso tra 0 e 1.");
				}
				float[] expSmooth = new float[a.Length];
				expSmooth[0] = a[0];
				for (int i = 0; i < expSmooth.Length - 1; i++)
				{
					expSmooth[i+1] = alpha * a[i] + (1 - alpha) * expSmooth[i];
				}
				return expSmooth;
			}

			/// <summary>
			/// Calcola la deviazione standard mobile dell'array a.
			/// </summary>
			/// <param name="a">Array di input su cui calcolare la deviazione standard.</param>
			/// <param name="window">Grado approssimazione. Un valore maggiore indica una precisione maggiore.</param>
			/// <returns>L'array contenente le deviazioni standard mobili.</returns>
			public static float[] FloatingStdDev(float[] a, int window)
			{
				return OperationExecution(a, window, StdDev);
			}

			/// <summary>
			/// Calcola la media mobile del vettore in input.
			/// </summary>
			/// <param name="a">Array in input.</param>
			/// <param name="window">Grado approssimazione. Un valore maggiore indica una precisione maggiore.</param>
			/// <returns>Un array contenente la media mobile.</returns>
			public static float[] FloatingMean(float[] a, int window)
			{
				return OperationExecution(a, window, Mean);
			}

			/// <summary>
			/// Esegue lo smoothing o calcola la deviazione standard mobile a seconda dell'operazione assegnata a 'method'.
			/// </summary>
			/// <param name="a">Campioni da approssimare.</param>
			/// <param name="window">Grado di approssimazione. Un valore maggiore indica uno smoothing più approssimativo.</param>
			/// <param name="method">Con method = Analysis.Mean verrà eseguito lo smoothing dei dati. 
			/// Con method = Analysis.StdDev verrà eseguita la deviazione standard mobile.</param>
			/// <returns>Un array di float contenente i valori approssimati.</returns>
			private static float[] OperationExecution(float[] a, int window, Dynamic method)
			{
				//if (window < 0 || window > a.Length)
				//{
				//	throw new IndexOutOfRangeException("Il parametro 'window' non può avere valori negativi o maggiori del numero di campioni.");
				//}
				float[] op = new float[a.Length];
				int sup = 0, inf = 0;
				for (int i = 0; i < a.Length; i++)
				{
					inf = i - window;
					sup = i + window;
					if (inf >= 0 && sup < a.Length)
					{
						op[i] = method(a, inf, sup);
					}
					else if (inf < 0 && sup < a.Length)
					{
						op[i] = method(a, 0, sup);
					}
					else if (inf >= 0 && sup >= a.Length)
					{
						op[i] = method(a, inf, a.Length - 1);
					}
					else //if(window >= campioni.Length)
					{
						op[i] = method(a, 0, a.Length - 1);
					}
				}
				return op;
			}

			/// <summary>
			/// Calcola la media dell'array float.
			/// </summary>
			/// <param name="a">Array contenete i dati su cui calcolare la media.</param>
			/// <returns>La media dei valori di a.</returns>
			public static float Mean(float[] a)
			{
				return Mean(a, 0, a.Length - 1);
			}

			/// <summary>
			/// Calcola la media dell'array float.
			/// </summary>
			/// <param name="a">Array contenete i dati su cui calcolare la media.</param>
			/// <param name="start">Indice di partenza (incluso).</param>
			/// <param name="end">Indice di fine (incluso).</param>
			/// <returns>La media dei valori appartenenti a a[start] e a[end].</returns>
			public static float Mean(float[] a, int start, int end)
			{
				if (start < 0 || start > end || end > a.Length)
				{
					throw new IndexOutOfRangeException("I parametri 'start' e 'end' hanno valori non accettabili.");
				}
				float sum = 0f;
				for (int i = start; i <= end; i++)
				{
					sum += a[i];
				}
				float mean = sum / (end - start + 1) ;
				return mean;
			}

			/// <summary>
			/// Calcola il rapporto incrementale.
			/// </summary>
			/// <param name="a">Array contenete i dati.</param>
			/// <param name="h">Intervallo minimo di spostamento sull'asse x.</param>
			/// <returns>Rapporto incrementale di ogni valore nell'array.</returns>
			public static float[] RIfunc(float[] a, float h)
			{
				if (h <= 0)
				{
					throw new ArgumentOutOfRangeException("Lo spostamento minimo deve essere maggiore di 0.");
				}
				float[] RI = new float[a.Length];
				for (int i = 0; i < a.Length; i++)
				{
					if (i + 1 < a.Length)
						RI[i] = (a[i + 1] - a[i]) / h;
					else
						RI[i] = 0;
				}
				return RI;
			}

			/// <summary>
			/// Calcola la deviazione standard.
			/// </summary>
			/// <param name="a">Array contenete i dati su cui calcolare la deviazione standard.</param>
			/// <returns>Deviazione standard dell'array in input.</returns>
			public static float StdDev(float[] a)
			{
				return StdDev(a, 0, a.Length - 1);
			}

			/// <summary>
			/// Calcola la deviazione standard dell'array float.
			/// </summary>
			/// <param name="a">Array contenete i dati su cui calcolare la deviazione standard.</param>
			/// <param name="start">Indice di partenza (incluso).</param>
			/// <param name="end">Indice di fine (incluso).</param>
			/// <returns>La deviazione standard dei valori appartenenti a a[start] e a[end].</returns>
			public static float StdDev(float[] a, int start, int end)
			{
				if (start < 0 || start > end || end > a.Length)
				{
					throw new IndexOutOfRangeException("I parametri 'start' e 'end' hanno valori non accettabili.");
				}
				double var = 0f;
				float mean = Mean(a, start, end);
				for (int i = start; i <= end; i++)
				{
					var += Math.Pow(a[i] - mean, 2.0d) / end;
				}
				return (float)Math.Sqrt(var);
			}

			/// <summary>
			/// Calcola l'integrale numerico di un array f.
			/// </summary>
			/// <param name="f">Array in input.</param>
			/// <param name="dt">Lo spostamento temporale tra un valore e l'altro.</param>
			/// <returns>L'integrale numerico.</returns>
			public static float Integrate(float[] f, float dt)
			{
				float integral = 0f;
                float a = Analysis.StdDev(f);
				for (int t = 0; t < f.Length; t++)
				{
					integral += a * dt;
				}

				return integral;
			}

			/// <summary>
			/// IV Funzionalità base.
			/// </summary>
			/// <param name="q0"></param>
			/// <param name="q1"></param>
			/// <param name="q2"></param>
			/// <param name="q3"></param>
			/// <returns></returns>
			private static float[] Triangolazione(float q0, float q1, float q2, float q3)
			{
				float[] orientamento = new float[3];
				orientamento[0] = Yaw(q0, q1, q2, q3);
				orientamento[1] = Pitch(q0, q1, q2, q3);
				orientamento[2] = Roll(q0, q1, q2, q3);

				return orientamento;
			}

			public static float Roll(float q0, float q1, float q2, float q3)
			{
				return (float)Math.Atan(((2 * q2 * q3) + (2 * q0 * q1)) /
					(2 * Math.Pow(q0, 2) + (2 * Math.Pow(q3, 2) - 1)));
			}

			public static float Pitch(float q0, float q1, float q2, float q3)
			{
				return -(float)Math.Asin((2 * q1 * q3) - (2 * q0 * q2));
			}

			public static float Yaw(float q0, float q1, float q2, float q3)
			{
				return (float)Math.Atan(((2 * q1 * q2) + (2 * q0 * q3)) /
					(2 * Math.Pow(q0, 2) + (2 * Math.Pow(q1, 2) - 1)));
			}

			/// <summary>
			/// Rimuove le discontinuità provocate da Math.Atan().
			/// </summary>
			/// <param name="a">Array in input.</param>
			/// <param name="delta">L'oggetto utilizzato per tenere conto delle discontinuità derivanti da finestre precedenti.</param>
			/// <returns>Un array senza discontinuità.</returns>
			public static float[] RemoveAtanDiscontinuity(float[] a, DiscontinuityHolder delta)
			{
				float[] res = new float[a.Length];
				for (int i = 0; i < a.Length; i++)
				{
					res[i] = a[i] + delta.sign;
					if (Math.Abs(a[i]) >= Thresholds.DISCONTINUITY && i < a.Length - 1)
					{
						if (Math.Sign(a[i]) != Math.Sign(a[i + 1]))
						{
							if (a[i] > a[i + 1])
							{
								delta.sign += (float)Math.PI;
							}
							else
							{
								delta.sign -= (float)Math.PI;
							}
						}
					}
				}
				return res;
			}

            /// <summary>
            /// Restituisce un int contenete lo stato sdraiato/seduto/inpiedi
            /// 1: sdraiato
            /// 2: sraiato/seduto
            /// 3: seduto
            /// 4: in piedi
            /// </summary>
            /// <param name="x">varaìiabile contenente il vaore dell'accellerazione dell'asse y</param>
            /// <returns>valore intero che indica lo stato</returns>
			public static PositionState LayStandSit(float y)
			{
				if (y > 8.4)
				{
					return PositionState.LAY;
				}
				if (y > 7.4 && y < 8.4)
				{
					return PositionState.LAYSIT;
				}
				else if (y > 2.7 && y < 7.4)
				{
					return PositionState.SIT;
				}
				else // y < 3.7
				{
					return PositionState.STAND;
				}
			}

			/// <summary>
			/// Estrae da m tutta la colonna relativa al sensore indicato.
			/// </summary>
			/// <param name="m">Matrice contenente i dati da estrarre.</param>
			/// <param name="sensor">Numero sensore da cui estrarre.</param>
			/// <returns>Array contenente i dati estratti.</returns>
			public static float[] ExtractSensorData(float[,] m, int sensor)
			{
				float[] tmp = new float[m.GetLength(0)]; // prende la colonna del tempo
				for (int t = 0; t < tmp.Length; t++)
				{
					tmp[t] = m[t, sensor];
				}

				return tmp;
			}

			/// <summary>
			/// Estrae i dati dalla matrice.
			/// </summary>
			/// <param name="m"></param>
			/// <param name="tool"></param>
			/// <param name="sensor"></param>
			/// <returns></returns>
			public static float[] ExtractArray(float[, ,] m, int tool, int sensor)
			{
				float[] tmp = new float[m.GetLength(1)]; // prende la colonna del tempo
				for (int t = 0; t < tmp.Length; t++)
				{
					tmp[t] = m[tool, t, sensor];
				}

				return tmp;
			}
			/// <summary>
			/// Calcola l'indice del massimo di a dove tutti i valori di a sono visti in valore assoluto.
			/// </summary>
			/// <param name="a">Array in input.</param>
			/// <returns>L'indice del massimo di a dove tutti i valori di a sono visti in valore assoluto.</returns>
			public static int IndexOfMax_Abs(float[] a)
			{
				float max = float.MinValue;
				int index = 0;
				for (int i = 0; i < a.Length; i++)
				{
					if (Math.Abs(a[i]) > max)
					{
						max = a[i];
						index = i;
					}
				}
				return index;
			}
			public static int IndexOfMin(float[] a)
			{
				float min = float.MaxValue;
				int index = 0;
				for (int i = 0; i < a.Length; i++)
				{
					if (a[i] < min)
					{
						min = a[i];
						index = i;
					}
				}
				return index;
			}
		}// End of class Analysis

		/// <summary>
		/// Salva il segno. E' una classe invece che una struct perchè 
		/// la classe è istanziata sullo heap e non viene sovrascritta ogni volta che 
		/// lo stack viene liberato (uscita dal metodo).
		/// </summary>
		public class DiscontinuityHolder
		{
			public float sign;
		}
	}
}
