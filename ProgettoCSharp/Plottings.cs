﻿using ProgettoCSharp.DataManagement;
using ProgettoCSharp.DataAnalysis;
using System;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace ProgettoCSharp
{
	namespace DataVisualization
	{
		public class Plotter
		{
			double counter;
			int frequency;
			IProgress<ChartPoint> chartProgress;
			IProgress<int> chartUpdate;
			IProgress<int> chartReset;
			IProgress<ChartPointDR> chartProgressDeadReckoning;

			ChartPointDR pointDR;

			public Plotter(int frequency, IProgress<ChartPoint> chartProgress, IProgress<int> chartUpdate, IProgress<int> chartReset, IProgress<ChartPointDR> chartProgressDeadReckoning)
			{
				this.chartProgress = chartProgress;
				this.chartUpdate = chartUpdate;
				this.chartReset = chartReset;
				this.chartProgressDeadReckoning = chartProgressDeadReckoning;
				chartReset.Report(0);
				this.frequency = frequency;
				counter = 0.02d;
				pointDR = new ChartPointDR(null);
				chartProgressDeadReckoning.Report(new ChartPointDR(new PointF[1]{new PointF(0f, 0f)}));
			}

			public void DrawWindowEventHandler(object sender, RefinedDataEventArgs args)
			{
				chartProgress.Report(new ChartPoint(args.modacc, (float)counter, frequency, "Accelerometro"));
				chartProgress.Report(new ChartPoint(args.modgyr, (float)counter, frequency, "Giroscopio"));
				chartProgress.Report(new ChartPoint(args.modmag, (float)counter, frequency, "Magnetometro"));
				chartProgress.Report(new ChartPoint(args.theta, (float)counter, frequency, "Theta"));
				if (args.deadReckoningMotion != null)
				{
					pointDR.points = args.deadReckoningMotion;
					chartProgressDeadReckoning.Report(pointDR);
				}
				counter += args.modacc.GetLength(0) * (1.0f / frequency);
				chartUpdate.Report(0);
			}
		}

		public struct ChartPointDR
		{
			public PointF[] points;
			
			public ChartPointDR(PointF[] p)
			{
				points = p;
			}
		}

		public struct ChartPoint
		{
		//	public PointF point;
			public float[,] Y;
			public float X;
			public float freq;
			public string tool;

			public ChartPoint(float[,] y, float x, float f, string t)
			{
				X = x;
				Y = y;
				freq = f;
				tool = t;
			}
		}
	}
}
