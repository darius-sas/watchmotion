﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ProgettoCSharp.DataVisualization;

namespace ProgettoCSharp
{
	public partial class Form1 : Form
	{
		_System sys;
		/// <summary>
		/// Inizializza la form e il sottosistema.
		/// </summary>
		public Form1()
		{
			InitializeComponent();
			outputTextBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\WatchMotion\\";
			charts.Add("Accelerometro", chart);
			charts.Add("Giroscopio", chart1);
			charts.Add("Magnetometro", chart2);
			charts.Add("Theta", chart3);
			checkBoxes.Add(checkBox1);
			checkBoxes.Add(checkBox2);
			checkBoxes.Add(checkBox3);
			checkBoxes.Add(checkBox4);
			checkBoxes.Add(checkBox5);
			foreach (var c in charts)
			{
				c.Value.ChartAreas[0].AxisX.ScaleView.Zoom(0, 5);
			}
			chart4.Series[0].Points.AddXY(0f, 0f);
			sys = new _System(Int32.Parse(portNumberTextBox.Text));
		}

		/// <summary>
		/// Istanzia i gestori dei callback per l'aggiornamento dei Control della Form 
		/// che permettono il suo utilizzo anche durante l'esecuzione del sottosistema sys.
		/// </summary>
		/// <!-- .NET non permette l'uso dei Control appartenenti a un Thread su un altro Thread.
		/// Bisogna usare il pattern TAP (Task-Based Asyncronous Pattern): stackoverflow.com/questions/661561/how-to-update-the-gui-from-another-thread-in-c
		/// Gestori:
		/// - chartProgress: inserimento dei punti nel grafico.
		/// - chartUpdate: aggiornamento del grafico. 
		/// - chartReset: reset del grafico. 
		/// - progress: aggiornamento della label con lo stato di connessione. -->
		private async void ListenButton_Click(object sender, EventArgs e)
		{
			if (fileNameTextBox.TextLength != 0)
				sys.FileName = fileNameTextBox.Text;
			richTextBox1.Text = "";
			sys.FilePath = outputTextBox.Text;
			SetInputControls(false);
			#region GUI Progress handler Tasks
			Progress<ChartPoint> chartProgress = new Progress<ChartPoint>(
				p =>
				{
					float tmp = p.X;
					for (int j = 0; j < p.Y.GetLength(1); j++)
					{
						// //social.msdn.microsoft.com/Forums/vstudio/en-US/f569382a-e05e-4295-b0f8-75cc4e504f79/add-scroll-bar-on-bar-chart-created-by-systemwindowsformsdatavisualizationchartingchart?forum=MSWinWebChart
						p.X = tmp;
						for (int i = 0; i < p.Y.GetLength(0); i++)
						{
							charts[p.tool].Series[String.Format(p.tool + " {0}", j + 1)].Points.AddXY(p.X, p.Y[i, j]);
							p.X += 1.0f / p.freq;
						}
					}
				});
			Progress<int> chartUpdate = new Progress<int>(i =>
			{
				double cp = 0.0;
				foreach (KeyValuePair<String, Chart> c in charts)
				{
					cp = c.Value.ChartAreas[0].AxisX.Maximum + 1d;
					c.Value.ChartAreas[0].AxisX.ScaleView.Scroll(cp);
					c.Value.Update();
				}
			});
			Progress<int> chartReset = new Progress<int>(
				i =>
				{
					foreach (KeyValuePair<String, Chart> c in charts)
					{
						foreach (var s in c.Value.Series)
						{
							s.Points.Clear();
						}
					}
					chart4.Series[0].Points.Clear();
				});
			Progress<ControlsState> progress = new Progress<ControlsState>(
				state =>
				{
					if (state.numberOfSensors != 5)
					{
						for (int i = state.numberOfSensors; i < checkBoxes.Count; i++)
						{
							checkBoxes[i].Checked = false;
							checkBoxes[i].Enabled = false;
						}
					}
					else if (state.logs != "")
					{
						richTextBox1.Text = richTextBox1.Text + state.logs;
						richTextBox1.SelectionStart = richTextBox1.Text.Length;
						richTextBox1.ScrollToCaret();
					}
					else
					{
						connectionStateLabel.Text = state.text;
						connectionStateLabel.ForeColor = state.color;
					}
				});
			Progress<ChartPointDR> chartProgressDeadReckoning = new Progress<ChartPointDR>(
				newPosition =>
				{
					for (int i = 0; i < newPosition.points.Length; i++)
					{
						chart4.Series[0].Points.AddXY(newPosition.points[i].X, newPosition.points[i].Y);
					}
				});
			#endregion
			await Task.Factory.StartNew(
				() => sys.Start(Int32.Parse(portNumberTextBox.Text), progress, chartProgress, chartUpdate, chartReset, chartProgressDeadReckoning),
				TaskCreationOptions.LongRunning);
			SetInputControls(true);
		}

		/// <summary>
		/// Interrompe l'esecuzione del sistema.
		/// </summary>
		private void AbortButton_Click(object sender, EventArgs e)
		{
			sys.Abort();
			SetInputControls(true);
			listenButton.Enabled = true;
			connectionStateLabel.Text = "Chiusa";
			connectionStateLabel.ForeColor = Color.Red;
		}

		/// <summary>
		/// Apre un Dialog per selezionare una cartella di output.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void outputButton_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderDialog = new FolderBrowserDialog();
			folderDialog.Description = "Scegliere la cartella dove salvare i file.";
			if (folderDialog.ShowDialog() == DialogResult.OK)
			{
				outputTextBox.Text = folderDialog.SelectedPath;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SensorCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox s = (CheckBox)sender;
			int sensor = Int32.Parse((string)s.Tag) - 1;
			foreach (KeyValuePair<string, Chart> c in charts)
			{
				if(sensor < c.Value.Series.Count)
					c.Value.Series[sensor].Enabled = !c.Value.Series[sensor].Enabled;
			}
		}

		/// <summary>
		/// Inverte la proprietà cb.Checked di tutte le checkbox relative ai sensori.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void checkingButton_Click(object sender, EventArgs e)
		{
			Button b = (Button)sender;
			foreach (var cb in checkBoxes)
			{
				if (cb.Enabled)
				{
					cb.Checked = !cb.Checked;
				}
			}
		}

		/// <summary>
		/// Invert la proprietà Enabled dei controlli di input presenti nella form.
		/// </summary>
		private void SetInputControls(bool enable)
		{
			fileNameTextBox.Enabled = enable;
			outputButton.Enabled = enable;
			outputTextBox.Enabled = enable;
			portNumberTextBox.Enabled = enable;
			listenButton.Enabled = enable;
		}

		/// <summary>
		/// Cambia le label delle checkbox al cambiamento del tab.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (tabControl1.SelectedIndex == 4)
			{
				groupBox2.Enabled = false;
			}
			else
			{
				groupBox2.Enabled = true;
			}
			if (tabControl1.SelectedIndex == 3)
			{
				checkBox1.Text = "Dati grezzi";
				checkBox2.Text = "Dati lavorati";
				checkBox3.Text = "Yaws lavorati";
				checkBox4.Text = "Acc Y";
				//checkBox4.Enabled = false;
				checkBox5.Enabled = false;
			}
			else
			{
				checkBox1.Text = "Sensore 1";
				checkBox2.Text = "Sensore 2";
				checkBox3.Text = "Sensore 3";
				checkBox4.Text = "Sensore 4";
				checkBox4.Enabled = true;
				checkBox5.Enabled = true;
			}
		}

		int zoom_interval = Int32.MinValue;
		void chart4_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(zoom_interval == Int32.MinValue)
				zoom_interval = (int) chart4.ChartAreas[0].AxisX.Maximum;
			if (e.Delta < 0)
			{
				//MessageBox.Show("Rotella giù");
				zoom_interval += 5;
				chart4.ChartAreas[0].AxisX.ScaleView.Zoom(-zoom_interval, zoom_interval);
				chart4.ChartAreas[0].AxisY.ScaleView.Zoom(-zoom_interval, zoom_interval);

				
			}
			else
			{
				//MessageBox.Show("Rotella su");
				if (zoom_interval == 0)
					return;
				zoom_interval -= 5;
				chart4.ChartAreas[0].AxisX.ScaleView.Zoom(-zoom_interval, zoom_interval);
				chart4.ChartAreas[0].AxisY.ScaleView.Zoom(-zoom_interval, zoom_interval);
			}
		}

		private void chart4_MouseEnter(object sender, EventArgs e)
		{
			if (!this.chart4.Focused)
				chart4.Focus();
		}

		private void chart4_MouseLeave(object sender, EventArgs e)
		{
			if (this.chart4.Focused)
				chart4.Parent.Focus();
		}

	}
}
