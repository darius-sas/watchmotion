﻿using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using System;
using System.Collections.Generic;
namespace ProgettoCSharp
{
	partial class Form1
	{
		/// <summary>
		/// Variabile di progettazione necessaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Pulire le risorse in uso.
		/// </summary>
		/// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Codice generato da Progettazione Windows Form

		/// <summary>
		/// Metodo necessario per il supporto della finestra di progettazione. Non modificare
		/// il contenuto del metodo con l'editor di codice.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.fileNameTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.outputButton = new System.Windows.Forms.Button();
			this.outputTextBox = new System.Windows.Forms.TextBox();
			this.abortButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.portNumberTextBox = new System.Windows.Forms.TextBox();
			this.connectionStateLabel = new System.Windows.Forms.Label();
			this.connectionLabel = new System.Windows.Forms.Label();
			this.listenButton = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkingButton = new System.Windows.Forms.Button();
			this.checkBox5 = new System.Windows.Forms.CheckBox();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.Accelerometro = new System.Windows.Forms.TabPage();
			this.Giroscopio = new System.Windows.Forms.TabPage();
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.Magnetometro = new System.Windows.Forms.TabPage();
			this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.Theta = new System.Windows.Forms.TabPage();
			this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.Accelerometro.SuspendLayout();
			this.Giroscopio.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			this.Magnetometro.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
			this.Theta.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
			this.SuspendLayout();
			// 
			// chart
			// 
			chartArea1.AlignmentStyle = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentStyles.Position;
			chartArea1.AxisX.Interval = 1D;
			chartArea1.AxisX.LabelStyle.Format = "{0:##0.0}s";
			chartArea1.AxisX.MajorGrid.Enabled = false;
			chartArea1.AxisX.ScaleView.SmallScrollMinSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Seconds;
			chartArea1.AxisX.ScaleView.SmallScrollSize = 1D;
			chartArea1.AxisX.ScaleView.SmallScrollSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Seconds;
			chartArea1.AxisX.ScrollBar.BackColor = System.Drawing.Color.DarkGray;
			chartArea1.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.Gray;
			chartArea1.AxisX.ScrollBar.IsPositionedInside = false;
			chartArea1.AxisX.ScrollBar.LineColor = System.Drawing.Color.Black;
			chartArea1.AxisX.TitleForeColor = System.Drawing.Color.Silver;
			chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
			chartArea1.BackColor = System.Drawing.Color.White;
			chartArea1.Name = "ChartArea1";
			chartArea1.Position.Auto = false;
			chartArea1.Position.Height = 70F;
			chartArea1.Position.Width = 90F;
			chartArea1.Position.Y = 25F;
			this.chart.ChartAreas.Add(chartArea1);
			legend1.BackColor = System.Drawing.Color.White;
			legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
			legend1.Name = "Legend1";
			this.chart.Legends.Add(legend1);
			this.chart.Location = new System.Drawing.Point(-1, -5);
			this.chart.Name = "chart";
			series1.ChartArea = "ChartArea1";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series1.Legend = "Legend1";
			series1.Name = "Accelerometro 1";
			series2.ChartArea = "ChartArea1";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series2.Legend = "Legend1";
			series2.Name = "Accelerometro 2";
			series3.ChartArea = "ChartArea1";
			series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series3.Legend = "Legend1";
			series3.Name = "Accelerometro 3";
			series4.ChartArea = "ChartArea1";
			series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series4.Legend = "Legend1";
			series4.Name = "Accelerometro 4";
			series5.ChartArea = "ChartArea1";
			series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series5.Legend = "Legend1";
			series5.Name = "Accelerometro 5";
			this.chart.Series.Add(series1);
			this.chart.Series.Add(series2);
			this.chart.Series.Add(series3);
			this.chart.Series.Add(series4);
			this.chart.Series.Add(series5);
			this.chart.Size = new System.Drawing.Size(668, 530);
			this.chart.TabIndex = 0;
			this.chart.Text = "chart1";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.fileNameTextBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.outputButton);
			this.groupBox1.Controls.Add(this.outputTextBox);
			this.groupBox1.Controls.Add(this.abortButton);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.portNumberTextBox);
			this.groupBox1.Controls.Add(this.connectionStateLabel);
			this.groupBox1.Controls.Add(this.connectionLabel);
			this.groupBox1.Controls.Add(this.listenButton);
			this.groupBox1.Location = new System.Drawing.Point(675, 1);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(198, 179);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Impostazioni";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(10, 94);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 17);
			this.label2.TabIndex = 11;
			this.label2.Text = "Percorso:";
			// 
			// fileNameTextBox
			// 
			this.fileNameTextBox.Location = new System.Drawing.Point(81, 71);
			this.fileNameTextBox.Name = "fileNameTextBox";
			this.fileNameTextBox.Size = new System.Drawing.Size(100, 20);
			this.fileNameTextBox.TabIndex = 10;
			this.fileNameTextBox.Text = "WatchMotion-data";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(10, 73);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 17);
			this.label1.TabIndex = 9;
			this.label1.Text = "Nome file:";
			// 
			// outputButton
			// 
			this.outputButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.outputButton.Location = new System.Drawing.Point(158, 114);
			this.outputButton.Name = "outputButton";
			this.outputButton.Size = new System.Drawing.Size(23, 20);
			this.outputButton.TabIndex = 8;
			this.outputButton.Text = "...";
			this.outputButton.UseVisualStyleBackColor = true;
			this.outputButton.Click += new System.EventHandler(this.outputButton_Click);
			// 
			// outputTextBox
			// 
			this.outputTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.outputTextBox.Location = new System.Drawing.Point(13, 114);
			this.outputTextBox.Name = "outputTextBox";
			this.outputTextBox.ReadOnly = true;
			this.outputTextBox.Size = new System.Drawing.Size(146, 20);
			this.outputTextBox.TabIndex = 7;
			// 
			// abortButton
			// 
			this.abortButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.abortButton.Location = new System.Drawing.Point(106, 141);
			this.abortButton.Name = "abortButton";
			this.abortButton.Size = new System.Drawing.Size(75, 25);
			this.abortButton.TabIndex = 6;
			this.abortButton.Text = "Annulla";
			this.abortButton.UseVisualStyleBackColor = true;
			this.abortButton.Click += new System.EventHandler(this.AbortButton_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(10, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(42, 17);
			this.label3.TabIndex = 5;
			this.label3.Text = "Porta:";
			// 
			// portNumberTextBox
			// 
			this.portNumberTextBox.Location = new System.Drawing.Point(115, 46);
			this.portNumberTextBox.MaxLength = 5;
			this.portNumberTextBox.Name = "portNumberTextBox";
			this.portNumberTextBox.Size = new System.Drawing.Size(66, 20);
			this.portNumberTextBox.TabIndex = 4;
			this.portNumberTextBox.Text = "45555";
			// 
			// connectionStateLabel
			// 
			this.connectionStateLabel.AutoSize = true;
			this.connectionStateLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.connectionStateLabel.ForeColor = System.Drawing.Color.Red;
			this.connectionStateLabel.Location = new System.Drawing.Point(113, 19);
			this.connectionStateLabel.Name = "connectionStateLabel";
			this.connectionStateLabel.Size = new System.Drawing.Size(46, 17);
			this.connectionStateLabel.TabIndex = 3;
			this.connectionStateLabel.Text = "Chiusa";
			// 
			// connectionLabel
			// 
			this.connectionLabel.AutoSize = true;
			this.connectionLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.connectionLabel.Location = new System.Drawing.Point(10, 19);
			this.connectionLabel.Name = "connectionLabel";
			this.connectionLabel.Size = new System.Drawing.Size(79, 17);
			this.connectionLabel.TabIndex = 2;
			this.connectionLabel.Text = "Connesione:";
			// 
			// listenButton
			// 
			this.listenButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listenButton.Location = new System.Drawing.Point(13, 141);
			this.listenButton.Name = "listenButton";
			this.listenButton.Size = new System.Drawing.Size(75, 25);
			this.listenButton.TabIndex = 1;
			this.listenButton.Text = "Ascolta";
			this.listenButton.UseVisualStyleBackColor = true;
			this.listenButton.Click += new System.EventHandler(this.ListenButton_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Checked = true;
			this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox1.Location = new System.Drawing.Point(13, 19);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(74, 17);
			this.checkBox1.TabIndex = 8;
			this.checkBox1.Tag = "1";
			this.checkBox1.Text = "Sensore 1";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.SensorCheckBox_CheckedChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.AutoSize = true;
			this.checkBox2.Checked = true;
			this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox2.Location = new System.Drawing.Point(13, 43);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(74, 17);
			this.checkBox2.TabIndex = 9;
			this.checkBox2.Tag = "2";
			this.checkBox2.Text = "Sensore 2";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.SensorCheckBox_CheckedChanged);
			// 
			// checkBox3
			// 
			this.checkBox3.AutoSize = true;
			this.checkBox3.Checked = true;
			this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox3.Location = new System.Drawing.Point(13, 66);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(74, 17);
			this.checkBox3.TabIndex = 10;
			this.checkBox3.Tag = "3";
			this.checkBox3.Text = "Sensore 3";
			this.checkBox3.UseVisualStyleBackColor = true;
			this.checkBox3.CheckedChanged += new System.EventHandler(this.SensorCheckBox_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkingButton);
			this.groupBox2.Controls.Add(this.checkBox5);
			this.groupBox2.Controls.Add(this.checkBox4);
			this.groupBox2.Controls.Add(this.checkBox3);
			this.groupBox2.Controls.Add(this.checkBox1);
			this.groupBox2.Controls.Add(this.checkBox2);
			this.groupBox2.Location = new System.Drawing.Point(675, 184);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(198, 136);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Visualizzazione";
			// 
			// checkingButton
			// 
			this.checkingButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkingButton.Location = new System.Drawing.Point(106, 19);
			this.checkingButton.Name = "checkingButton";
			this.checkingButton.Size = new System.Drawing.Size(75, 25);
			this.checkingButton.TabIndex = 12;
			this.checkingButton.Text = "Switch";
			this.checkingButton.UseVisualStyleBackColor = true;
			this.checkingButton.Click += new System.EventHandler(this.checkingButton_Click);
			// 
			// checkBox5
			// 
			this.checkBox5.AutoSize = true;
			this.checkBox5.Checked = true;
			this.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox5.Location = new System.Drawing.Point(13, 113);
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.Size = new System.Drawing.Size(74, 17);
			this.checkBox5.TabIndex = 12;
			this.checkBox5.Tag = "5";
			this.checkBox5.Text = "Sensore 5";
			this.checkBox5.UseVisualStyleBackColor = true;
			this.checkBox5.CheckedChanged += new System.EventHandler(this.SensorCheckBox_CheckedChanged);
			// 
			// checkBox4
			// 
			this.checkBox4.AutoSize = true;
			this.checkBox4.Checked = true;
			this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox4.Location = new System.Drawing.Point(13, 90);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new System.Drawing.Size(74, 17);
			this.checkBox4.TabIndex = 11;
			this.checkBox4.Tag = "4";
			this.checkBox4.Text = "Sensore 4";
			this.checkBox4.UseVisualStyleBackColor = true;
			this.checkBox4.CheckedChanged += new System.EventHandler(this.SensorCheckBox_CheckedChanged);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.Accelerometro);
			this.tabControl1.Controls.Add(this.Giroscopio);
			this.tabControl1.Controls.Add(this.Magnetometro);
			this.tabControl1.Controls.Add(this.Theta);
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Location = new System.Drawing.Point(1, 1);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(668, 529);
			this.tabControl1.TabIndex = 12;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
			// 
			// Accelerometro
			// 
			this.Accelerometro.Controls.Add(this.chart);
			this.Accelerometro.Location = new System.Drawing.Point(4, 22);
			this.Accelerometro.Name = "Accelerometro";
			this.Accelerometro.Padding = new System.Windows.Forms.Padding(3);
			this.Accelerometro.Size = new System.Drawing.Size(660, 503);
			this.Accelerometro.TabIndex = 0;
			this.Accelerometro.Text = "Accelerometro";
			this.Accelerometro.UseVisualStyleBackColor = true;
			// 
			// Giroscopio
			// 
			this.Giroscopio.Controls.Add(this.chart1);
			this.Giroscopio.Location = new System.Drawing.Point(4, 22);
			this.Giroscopio.Name = "Giroscopio";
			this.Giroscopio.Padding = new System.Windows.Forms.Padding(3);
			this.Giroscopio.Size = new System.Drawing.Size(660, 503);
			this.Giroscopio.TabIndex = 1;
			this.Giroscopio.Text = "Giroscopio";
			this.Giroscopio.UseVisualStyleBackColor = true;
			// 
			// chart1
			// 
			chartArea2.AxisX.Interval = 0.5D;
			chartArea2.AxisX.LabelStyle.Format = "{0:##0.0}s";
			chartArea2.AxisX.MajorGrid.Enabled = false;
			chartArea2.AxisX.ScrollBar.IsPositionedInside = false;
			chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
			chartArea2.Name = "ChartArea2";
			chartArea2.Position.Auto = false;
			chartArea2.Position.Height = 70F;
			chartArea2.Position.Width = 90F;
			chartArea2.Position.X = 1F;
			chartArea2.Position.Y = 25F;
			this.chart1.ChartAreas.Add(chartArea2);
			legend2.Name = "Legend1";
			this.chart1.Legends.Add(legend2);
			this.chart1.Location = new System.Drawing.Point(-4, -14);
			this.chart1.Name = "chart1";
			series6.ChartArea = "ChartArea2";
			series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series6.Legend = "Legend1";
			series6.Name = "Giroscopio 1";
			series7.ChartArea = "ChartArea2";
			series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series7.Legend = "Legend1";
			series7.Name = "Giroscopio 2";
			series8.ChartArea = "ChartArea2";
			series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series8.Legend = "Legend1";
			series8.Name = "Giroscopio 3";
			series9.ChartArea = "ChartArea2";
			series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series9.Legend = "Legend1";
			series9.Name = "Giroscopio 4";
			series10.ChartArea = "ChartArea2";
			series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series10.Legend = "Legend1";
			series10.Name = "Giroscopio 5";
			this.chart1.Series.Add(series6);
			this.chart1.Series.Add(series7);
			this.chart1.Series.Add(series8);
			this.chart1.Series.Add(series9);
			this.chart1.Series.Add(series10);
			this.chart1.Size = new System.Drawing.Size(668, 530);
			this.chart1.TabIndex = 1;
			this.chart1.Text = "chart1";
			// 
			// Magnetometro
			// 
			this.Magnetometro.Controls.Add(this.chart2);
			this.Magnetometro.Location = new System.Drawing.Point(4, 22);
			this.Magnetometro.Name = "Magnetometro";
			this.Magnetometro.Size = new System.Drawing.Size(660, 503);
			this.Magnetometro.TabIndex = 2;
			this.Magnetometro.Text = "Magnetometro";
			this.Magnetometro.UseVisualStyleBackColor = true;
			// 
			// chart2
			// 
			chartArea3.AxisX.Interval = 0.5D;
			chartArea3.AxisX.LabelStyle.Format = "{0:##0.0}s";
			chartArea3.AxisX.MajorGrid.Enabled = false;
			chartArea3.AxisX.ScrollBar.IsPositionedInside = false;
			chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
			chartArea3.AxisY.MajorTickMark.Enabled = false;
			chartArea3.AxisY.MinorGrid.LineColor = System.Drawing.Color.DimGray;
			chartArea3.Name = "ChartArea3";
			chartArea3.Position.Auto = false;
			chartArea3.Position.Height = 70F;
			chartArea3.Position.Width = 90F;
			chartArea3.Position.X = 1F;
			chartArea3.Position.Y = 25F;
			this.chart2.ChartAreas.Add(chartArea3);
			legend3.Name = "Legend1";
			this.chart2.Legends.Add(legend3);
			this.chart2.Location = new System.Drawing.Point(-4, -14);
			this.chart2.Name = "chart2";
			series11.ChartArea = "ChartArea3";
			series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series11.Legend = "Legend1";
			series11.Name = "Magnetometro 1";
			series12.ChartArea = "ChartArea3";
			series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series12.Legend = "Legend1";
			series12.Name = "Magnetometro 2";
			series13.ChartArea = "ChartArea3";
			series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series13.Legend = "Legend1";
			series13.Name = "Magnetometro 3";
			series14.ChartArea = "ChartArea3";
			series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series14.Legend = "Legend1";
			series14.Name = "Magnetometro 4";
			series15.ChartArea = "ChartArea3";
			series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series15.Legend = "Legend1";
			series15.Name = "Magnetometro 5";
			this.chart2.Series.Add(series11);
			this.chart2.Series.Add(series12);
			this.chart2.Series.Add(series13);
			this.chart2.Series.Add(series14);
			this.chart2.Series.Add(series15);
			this.chart2.Size = new System.Drawing.Size(668, 530);
			this.chart2.TabIndex = 1;
			this.chart2.Text = "chart1";
			// 
			// Theta
			// 
			this.Theta.Controls.Add(this.chart3);
			this.Theta.Location = new System.Drawing.Point(4, 22);
			this.Theta.Name = "Theta";
			this.Theta.Size = new System.Drawing.Size(660, 503);
			this.Theta.TabIndex = 3;
			this.Theta.Text = "Theta";
			this.Theta.UseVisualStyleBackColor = true;
			// 
			// chart3
			// 
			chartArea4.AxisX.Interval = 0.5D;
			chartArea4.AxisX.LabelStyle.Format = "{0:##0.0}s";
			chartArea4.AxisX.MajorGrid.Enabled = false;
			chartArea4.AxisX.ScrollBar.IsPositionedInside = false;
			chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
			chartArea4.Name = "ChartArea4";
			chartArea4.Position.Auto = false;
			chartArea4.Position.Height = 70F;
			chartArea4.Position.Width = 90F;
			chartArea4.Position.X = 1F;
			chartArea4.Position.Y = 25F;
			this.chart3.ChartAreas.Add(chartArea4);
			legend4.Name = "Legend1";
			this.chart3.Legends.Add(legend4);
			this.chart3.Location = new System.Drawing.Point(-4, -14);
			this.chart3.Name = "chart3";
			series16.ChartArea = "ChartArea4";
			series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series16.Legend = "Legend1";
			series16.Name = "Theta 1";
			series17.ChartArea = "ChartArea4";
			series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series17.Legend = "Legend1";
			series17.Name = "Theta 2";
			series18.ChartArea = "ChartArea4";
			series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series18.Legend = "Legend1";
			series18.Name = "Theta 3";
			series19.ChartArea = "ChartArea4";
			series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series19.Legend = "Legend1";
			series19.Name = "Theta 4";
			series20.ChartArea = "ChartArea4";
			series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series20.Legend = "Legend1";
			series20.Name = "Theta 5";
			this.chart3.Series.Add(series16);
			this.chart3.Series.Add(series17);
			this.chart3.Series.Add(series18);
			this.chart3.Series.Add(series19);
			this.chart3.Series.Add(series20);
			this.chart3.Size = new System.Drawing.Size(668, 530);
			this.chart3.TabIndex = 2;
			this.chart3.Text = "chart1";
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.chart4);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(660, 503);
			this.tabPage1.TabIndex = 4;
			this.tabPage1.Text = "Dead Reckoning";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// chart4
			// 
			chartArea5.AxisX.Crossing = 0D;
			chartArea5.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
			chartArea5.AxisX.LabelStyle.Format = "{00}";
			chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
			chartArea5.AxisX.Maximum = 50D;
			chartArea5.AxisX.Minimum = -50D;
			chartArea5.AxisX.ScrollBar.IsPositionedInside = false;
			chartArea5.AxisX2.LabelStyle.Format = "{00.0}";
			chartArea5.AxisX2.ScrollBar.IsPositionedInside = false;
			chartArea5.AxisY.Crossing = 0D;
			chartArea5.AxisY.LabelStyle.Format = "{00}";
			chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
			chartArea5.AxisY.Maximum = 50D;
			chartArea5.AxisY.Minimum = -50D;
			chartArea5.Name = "DeadReckoning";
			chartArea5.Position.Auto = false;
			chartArea5.Position.Height = 70F;
			chartArea5.Position.Width = 85F;
			chartArea5.Position.X = 5F;
			chartArea5.Position.Y = 20F;
			this.chart4.ChartAreas.Add(chartArea5);
			this.chart4.Cursor = System.Windows.Forms.Cursors.Hand;
			legend5.Name = "Legend1";
			this.chart4.Legends.Add(legend5);
			this.chart4.Location = new System.Drawing.Point(-4, -14);
			this.chart4.Name = "chart4";
			series21.ChartArea = "DeadReckoning";
			series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series21.Legend = "Legend1";
			series21.Name = "DeadReckoning";
			series21.YValuesPerPoint = 3;
			this.chart4.Series.Add(series21);
			this.chart4.Size = new System.Drawing.Size(668, 530);
			this.chart4.TabIndex = 3;
			this.chart4.Text = "chart1";
			this.chart4.MouseEnter += new System.EventHandler(this.chart4_MouseEnter);
			this.chart4.MouseLeave += new System.EventHandler(this.chart4_MouseLeave);
			this.chart4.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.chart4_MouseWheel);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.CausesValidation = false;
			this.richTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBox1.ForeColor = System.Drawing.Color.Lime;
			this.richTextBox1.Location = new System.Drawing.Point(675, 347);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(198, 179);
			this.richTextBox1.TabIndex = 13;
			this.richTextBox1.Text = "";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(675, 327);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(33, 17);
			this.label4.TabIndex = 12;
			this.label4.Text = "Log:";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(879, 530);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Form1";
			this.Text = "WatchMotion";
			((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.Accelerometro.ResumeLayout(false);
			this.Giroscopio.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			this.Magnetometro.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
			this.Theta.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
			this.tabPage1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Chart chart;
		private Chart chart1;
		private Chart chart2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button abortButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox portNumberTextBox;
		private System.Windows.Forms.Label connectionStateLabel;
		private System.Windows.Forms.Label connectionLabel;
		private System.Windows.Forms.Button listenButton;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.Button outputButton;
		private System.Windows.Forms.TextBox outputTextBox;
		private System.Windows.Forms.TextBox fileNameTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage Accelerometro;
		private System.Windows.Forms.TabPage Giroscopio;
		private System.Windows.Forms.TabPage Magnetometro;
		private List<System.Windows.Forms.CheckBox> checkBoxes = new List<System.Windows.Forms.CheckBox>(5);
		private Dictionary<string, Chart> charts = new Dictionary<string, Chart>(3);
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button checkingButton;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TabPage Theta;
		private Chart chart3;
		private System.Windows.Forms.TabPage tabPage1;
		private Chart chart4;
	}
}

