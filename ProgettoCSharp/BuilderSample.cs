﻿using System;
using System.Threading;

namespace ProgettoCSharp
{
	namespace DataManagement
	{
		#region Utility

		/// <summary>
		/// Delegate per la gestione dell'evento di disegno del grafico.
		/// </summary>
		/// <param name="sender">L'oggetto che ha scatenato l'evento.</param>
		/// <param name="args">Classe che incapsula gli argomenti da passare all'oggetto che disegna il grafico.</param>
		public delegate void SampleWindowFull(object sender, SampleWindowFullEventArgs e);

		public class SampleWindowFullEventArgs : EventArgs
		{
			public float[, ,] MainWindow { get; private set; }
			public int NumberOfSensors { get; private set; }
			public int CurrentTime { get; private set; }
			public int Frequency { get; private set; }
			public SampleWindowFullEventArgs(float[, ,] MainWindow, int Frequency, int NumberOfSensors, int CurrentTime)
			{
				this.MainWindow = MainWindow;
				this.NumberOfSensors = NumberOfSensors;
				this.CurrentTime = CurrentTime;
				this.Frequency = Frequency;
			}
		}
		#endregion

		#region Classi SampleBuilder e SampleWindow
		/// <summary>
		/// Legenda:
		/// Sensor = Uno dei 5 sensori.
		/// Tool = Uno tra Accelerometro, Giroscopio, Magnetometro (3 Dimenensioni) e Quaternoni (4 Dimensioni).  
		/// </summary>
		public class SampleBuilder
		{
			int frequency;
			int numberOfSensors;
			int startIndex;
			int totalSamples;
			float[, ,] mainWindow;
			System.Text.StringBuilder csvString;

			/// <summary>
			/// Indice ultimo elemento scritto nella finestra.
			/// </summary>
			public int CurrentTime { get; set; }
			public event SampleWindowFull WindowFull;

			/// <summary>
			/// Inizializza il builder con la giusta Frequenza.
			/// </summary>
			/// <param name="Hz">Frequenza del campionamento.</param>
			public SampleBuilder(int Hz)
			{
				frequency = Hz;
				numberOfSensors = 5; // Assumo che i sensori siano 5
				startIndex = 7;
				CurrentTime = 0;
				totalSamples = (int)(frequency * Constants.SAMPLE_WINDOW_LENGTH);
				mainWindow = new float[Constants.TOOLS_NUMBER, totalSamples, numberOfSensors];
				csvString = new System.Text.StringBuilder(75000); // 75000 caratteri, buffer iniziale
			}
			[Obsolete("Deprecato. Meglio utilizzare quello con un solo argomento.")]
			public SampleBuilder(int Hz, int numberOfSensors)
			{
				this.frequency = Hz;
				this.numberOfSensors = numberOfSensors;
				if (numberOfSensors == 5)
				{
					startIndex = 7;
				}
				else
				{
					startIndex = 5;
				}
			}

			/// <summary>
			/// Converte i dati e costruisce l'istanza di SampleWindow che permetterà di analizzare i dati.
			/// </summary>
			/// <param name="packet">Pacchetto da convertire e memorizzare.</param>
			public void BuildSample(byte[] packet)
			{
				int tmpIndex = startIndex;
				float sample = 0f;
				for (int sensor = 0; sensor < numberOfSensors; ++sensor)
				{
					for (int tool = 0; tool < Constants.TOOLS_NUMBER; ++tool)
					{
						if (BitConverter.IsLittleEndian)
							Array.Reverse(packet, tmpIndex, 4);
						sample = BitConverter.ToSingle(packet, tmpIndex);
						AddSample(tool, sensor, sample);
						tmpIndex += 4; //incremento di 4 per andare al dato successivo
					}
				}
				CurrentTime++;
			}

			/// <summary>
			/// Aggiunge un dato alla finestra (SampleWindow).
			/// </summary>
			/// <param name="tool">Tra 0 e 2 è un accelerometro, tra 3 e 5 è un giroscopio e tra 6 e 8 è un magnetometro. Tra 9 e 12 sono i quaternioni.</param>
			/// <param name="sensor">Il numero del sensore, comprso tra 0 e 8.</param>
			/// <param name="sample">Il dato effettivo campionato dal tool.</param>
			private void AddSample(int tool, int sensor, float sample)
			{
				if (CurrentTime == totalSamples)
				{
					var args = new SampleWindowFullEventArgs(mainWindow, frequency, numberOfSensors, CurrentTime);
					IAsyncResult wfres = WindowFull.BeginInvoke(this, args, null, null);
					BuildCsvString(CurrentTime);
					CurrentTime = 0;
					WindowFull.EndInvoke(wfres);
				}
				mainWindow[tool, CurrentTime, sensor] = sample;
			}

			/// <summary>
			/// A ricezione finita, in caso la finestra non si sia riempita triggera l'evento WindowFull.
			/// </summary>
			public void ClearWindow()
			{
				if (mainWindow != null && CurrentTime != 0)
				{
					var args = new SampleWindowFullEventArgs(mainWindow, frequency, numberOfSensors, CurrentTime);
					WindowFull(this, args);
					BuildCsvString(CurrentTime);
				}
			}

			/// <summary>
			/// Aggiorna il numero di sensori in caso quando vengono ricevuti i pacchetti si scopre che sia diverso da 5.
			/// </summary>
			/// <param name="numberOfSensors">Il numero di sensori del campionamento.</param>
			public void UpdateNumberOfSensors(int numberOfSensors)
			{
				this.numberOfSensors = numberOfSensors;
				startIndex = 5;
			}

			/// <summary>
			/// Costruisce man mano la stringa per scrivere il file. Viene invocato ogni volta che la finestra è piena.
			/// </summary>
			void BuildCsvString(object currentWindowSize)
			{
				int size = (int)currentWindowSize;
				for (int time = 0; time < size; time++)
				{
					for (int sensor = 0; sensor < 5; sensor++)
					{
						for (int tool = 0; tool < Constants.TOOLS_NUMBER; tool++) // tool = misuratore
						{
							csvString.AppendFormat("{0};", mainWindow[tool, time, sensor]);
						}
						csvString.Append(";");
					}
					csvString.Append(Environment.NewLine);
				}
			}

			/// <summary>
			/// Scrive su file tutti i dati ricevuti.
			/// </summary>
			/// <param name="fileName">Il nome del file.</param>
			public string GetCsvString()
			{
				return csvString.ToString();
			}
		}
		#endregion
	}
}
