﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgettoCSharp.DataAnalysis;

namespace ProgettoCSharp
{
    public class Sensor
    {
        float[, ,] matrice;
        float[,] moddacc;
        float[,] moddgyr;
        float[,] moddmag;
        float stdacc;
        float stdgyr;
        float stdmag;

        public int Sensonr { get; private set; }

        public Sensor(float[,,] mat, float[,] acc, float[,] gyr, float[,] mag, int s) {
            matrice = mat;
            moddacc = acc;
            moddgyr = gyr;
            moddmag = mag;
            Sensonr = s;
        }

        public void Modulo(float[,] matrice2, int indice){
            for (int t = 0; t < matrice.GetLength(1); t++)
            {
                matrice2[t, Sensonr] = Analysis.Module(matrice[indice, t, Sensonr],
                    matrice[indice+1, t, Sensonr],
                    matrice[indice+2, t, Sensonr]);
            }
            /*float[] array;
            array = Analysis.Module(matrice, indice, Sensonr);
            for (int i = 0; i < matrice.GetLength(1); i++)
            {
                matrice2[i, Sensonr] = array[i];
            }*/
        }

        public void ModAccelerometro()
        {
            Modulo(moddacc, 0);
        }

        public void ModGiroscopio()
        {
            Modulo(moddgyr, 3);
        }

        public void ModMagnatometro()
        {
            Modulo(moddmag, 6);
        }

        public void Std(float[,] matrice2, int indice)
        {
            float[] arrayAcc = new float[moddacc.Length];
            float[] arrayGyr = new float[moddgyr.Length];
            float[] arrayMag = new float[moddmag.Length];

            for (int i = 0; i < moddacc.Length; i++)
			{
			    arrayAcc[i]=moddacc[i,Sensonr];
                arrayGyr[i]=moddgyr[i,Sensonr];
                arrayMag[i]=moddmag[i,Sensonr];
			}
            stdacc = Analysis.StdDev(arrayAcc);
            stdgyr = Analysis.StdDev(arrayGyr);
            stdmag = Analysis.StdDev(arrayMag);
        }


    }
}