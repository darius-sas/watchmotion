﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ProgettoCSharp.DataManagement;

namespace ProgettoCSharp
{
	namespace DataAnalysis
	{
		public abstract class AbstractRecognizer
		{
			public event DataAnalized DataRefined;
			protected RefinedDataEventArgs currentWindowData;
			protected StringBuilder motionStringBuilder;
			protected StringBuilder layStandSitBuilder;
			protected int numberOfSensors;
			protected float turnAngleRadiants;
			public int TurnAngle { get { return (int)(turnAngleRadiants * 180 / Math.PI); } }
			protected PositionState positionState;
			protected Sensor[] sensors;
			protected Thread[] threads;
			protected List<ActionListEntry> actionList;
            protected DiscontinuityHolder s;
            String str;
            int time;

			public AbstractRecognizer()
			{
				Analysis.sign = 0f;
				numberOfSensors = 5; // DefaultValue
				motionStringBuilder = new StringBuilder();
				layStandSitBuilder = new StringBuilder();
				sensors = new Sensor[numberOfSensors];
				threads = new Thread[sensors.Length];
				actionList = new List<ActionListEntry>();
                s = new DiscontinuityHolder();
			}

			/// <summary>
			/// Gestore evento che è responsabile di eseguire tutte le analisi e sollevare l'evento DataRefined al completamento.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="args"></param>
			public void AnalyzeData_WindowFull(object sender, SampleWindowFullEventArgs args)
			{
				float[,] modacc = new float[args.CurrentTime, args.NumberOfSensors];
				float[,] modgyr = new float[args.CurrentTime, args.NumberOfSensors];
				float[,] modmag = new float[args.CurrentTime, args.NumberOfSensors];
				float[,] arctan = new float[args.CurrentTime, args.NumberOfSensors];
				RefinedDataEventArgs previousWindowData = currentWindowData;
				currentWindowData = new RefinedDataEventArgs(args.MainWindow, modacc, modgyr, modmag, arctan);

				for (int i = 0; i < sensors.Length; i++)
				{
					sensors[i] = new Sensor(i, currentWindowData);
					threads[i] = new Thread(sensors[i].CalculateModules);
					threads[i].Start();
				}

				foreach (Thread t in threads) // Aspetta che tutti i thread abbiano finito i calcoli
				{
					t.Join();
				}

				Recognize();
				DeadReckoning(args.Frequency);
				DataRefined(this, currentWindowData);
				Action[] currentActions = new Action[sensors.Length];
				for (int i = 0; i < currentActions.Length; i++)
				{
					currentActions[i] = sensors[i].GetAction();
				}
				ActionListEntry entry = new ActionListEntry(currentActions, positionState, TurnAngle);
				actionList.Add(entry);
                CheckMiddleWindowsEvents(previousWindowData);
			}

			public abstract String GetDataAnalysisResult(int frequency);
			protected abstract void Recognize();
			protected abstract void CalculateTurnAngle(float[] theta);
			protected abstract PositionState IsLayStandSit(float[, ,] mat);
			protected abstract void DeadReckoning(int frequency);

			/// <summary>
			/// Controlla che non si sono persi eventi tra la finestra corrente e quella precedente. Da eseguire dopo l'analisi della finestra corrente.
			/// </summary>
			/// <param name="previousWindowData">La vecchia finestra.</param>
			private void CheckMiddleWindowsEvents(RefinedDataEventArgs previousWindowData)
			{
                
				if (previousWindowData == null || currentWindowData == null)
					return; // Nessuna finestra precedente
				if (previousWindowData.sampleWin.GetLength(1) / 2 > currentWindowData.modacc.GetLength(0)) // Dati insufficienti per effettuare analisi di metà finestra.
					return;
				float[, ,] sampWin = new float[previousWindowData.sampleWin.GetLength(0), previousWindowData.sampleWin.GetLength(1), previousWindowData.sampleWin.GetLength(2)];
				float[,] modacc = new float[previousWindowData.sampleWin.GetLength(1), previousWindowData.sampleWin.GetLength(2)];
				float[,] modgyr = new float[previousWindowData.sampleWin.GetLength(1), previousWindowData.sampleWin.GetLength(2)];
				float[,] modmag = new float[previousWindowData.sampleWin.GetLength(1), previousWindowData.sampleWin.GetLength(2)];
				float[,] theta = new float[previousWindowData.sampleWin.GetLength(1), previousWindowData.sampleWin.GetLength(2)];

				int tmp = 0;
				for (int i = 0; i < previousWindowData.sampleWin.GetLength(2); i++)
				{
                    for (int j = 0; j < currentWindowData.modacc.GetLength(0) / 2 - 1; j++)
					{
						tmp = j;
                        j += currentWindowData.modacc.GetLength(0) / 2 - 1;
						modacc[tmp, i] = previousWindowData.modacc[j, i];
						modgyr[tmp, i] = previousWindowData.modgyr[j, i];
						modmag[tmp, i] = previousWindowData.modmag[j, i];
						theta[tmp, i] = previousWindowData.theta[j, i];

						modacc[j, i] = currentWindowData.modacc[tmp, i];
						modgyr[j, i] = currentWindowData.modgyr[tmp, i];
						modmag[j, i] = currentWindowData.modmag[tmp, i];
						theta[j, i] = currentWindowData.theta[tmp, i];

						for (int k = 0; k < previousWindowData.sampleWin.GetLength(0); k++)
						{
							sampWin[k, j, i] = currentWindowData.sampleWin[k, tmp, i];
							sampWin[k, tmp, i] = previousWindowData.sampleWin[k, j, i];
						}
						j = tmp;
					}
					//tmp = 0;
					//for (int j = currentWindowData.sampleWin.GetLength(1) / 2; j < currentWindowData.sampleWin.GetLength(1); j++)
					//{
					//    modacc[j, i] = currentWindowData.modacc[tmp, i];
					//    modgyr[j, i] = currentWindowData.modgyr[tmp, i];
					//    modmag[j, i] = currentWindowData.modmag[tmp, i];
					//    theta[j, i] = currentWindowData.theta[tmp, i];
					//    for (int k = 0; k < currentWindowData.sampleWin.GetLength(0); k++)
					//        sampWin[k, j, i] = previousWindowData.sampleWin[k, tmp, i];
					//    tmp++;
					//}
				}

				RefinedDataEventArgs middleWindowData = new RefinedDataEventArgs(sampWin, modacc, modgyr, modmag, theta);
				Sensor[] midSensors = new Sensor[sampWin.GetLength(2)];
                Sensor[] preSensors = new Sensor[sampWin.GetLength(2)];
                bool inserire = false;
                for (int i = 0; i < sensors.Length; i++)
                {
                    midSensors[i] = new Sensor(i, middleWindowData);
                    preSensors[i] = new Sensor(i, previousWindowData);
                }

				for (int i = 0; i < sensors.Length; i++)
				{
                    bool prima, mezzo, dopo;

                    prima = preSensors[i].IsMoving();
                    mezzo = midSensors[i].IsMoving();
                    dopo = actionList[actionList.Count - 1].actions[i].motion;

                    if (prima != mezzo && mezzo != dopo)
                        inserire = true;
				}

                if (inserire)
                {
                    Action[] currentActions = new Action[sensors.Length];
                    for (int i = 0; i < currentActions.Length; i++)
                    {
                        currentActions[i] = midSensors[i].GetAction();
                    }
                    ActionListEntry entry = new ActionListEntry(currentActions, positionState, TurnAngle);
                    actionList.Insert(actionList.Count - 2, entry);
                }
			}
		
			/// <summary>
			/// Aggiorna il numero di sensori.
			/// </summary>
			/// <param name="numberOfSensors">Nuovo numero dei sensori.</param>
			public void UpdateNumberOfSensors(int numberOfSensors)
			{
				this.numberOfSensors = numberOfSensors;
				sensors = new Sensor[numberOfSensors];
				threads = new Thread[sensors.Length];
			}

			public StringBuilder GetMotionStringBuilder()
			{
				return motionStringBuilder;
			}

			public StringBuilder GetLayStandSitStringBuilder()
			{
				return layStandSitBuilder;
			}

            public void WriteOnFile(string content, string path)
            {
                System.Security.Permissions.FileIOPermission perm = new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.AllAccess, path);
                perm.Assert();
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path, true))
                {
                    sw.WriteLine(content);
                }
            }
		}
	}
}
