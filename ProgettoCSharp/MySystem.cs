﻿using ProgettoCSharp.DataAnalysis;
using ProgettoCSharp.DataManagement;
using ProgettoCSharp.DataVisualization;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ProgettoCSharp
{
	public class _System
	{
		int port;
		int lastLogIndex;
		Director dir;
		Plotter myPlotter;
		AbstractRecognizer dataRecognizer;
		IProgress<ControlsState> progress;
        string filePath;
        public string FilePath
        {
            get { return filePath; }
            set
            {
                if (!Directory.Exists(value))
                {
                    Directory.CreateDirectory(value);
                }
                filePath = value;
            }
        }
		public string FileName { get; set; }
		public string FileExt { get; set; }

		public _System(int port)
		{
			this.port = port;
			lastLogIndex = 0;
			dir = new Director(port);
			dir.DataReceivedEvent += dir_DataReceivedEvent;
			dir.DifferentNumberOfSensorEvent += dir_DifferentNumberOfSensorEvent;
			FilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\";
			FileName = "WMdatafile";
			FileExt = ".csv";
		}

		/// <summary>
		/// Esegue il sistema.
		/// </summary>
		/// <param name="progress">Progress per l'aggiornamento della label con lo stato di connessione.</param>
		/// <param name="chartProgress">Progress per l'inserimento dei punti nel grafico.</param>
		/// <param name="chartUpdate">Progress per l'aggiornamento del grafico.</param>
		/// <param name="chartReset">Progress per il reset del grafico.</param>
		public void Start(IProgress<ControlsState> progress,
			IProgress<ChartPoint> chartProgress,
			IProgress<int> chartUpdate,
			IProgress<int> chartReset,
			IProgress<ChartPointDR> chartProgressDeadReckoning)
		{
			this.progress = progress;
			lastLogIndex = 0;
			try
			{
				progress.Report(new ControlsState("In ascolto", Color.Yellow, s:5));
				if (!dir.SetUpConnection())
				{
					MessageBox.Show("Connessione interrotta.");
					return;
				}
				progress.Report(new ControlsState("Connesso", Color.Green));
				myPlotter = new Plotter(dir.Frequency, chartProgress, chartUpdate, chartReset, chartProgressDeadReckoning);
				
				dataRecognizer = new Recognizer();				
				dir.GetBuilder().WindowFull += dataRecognizer.AnalyzeData_WindowFull;
				dataRecognizer.DataRefined += myPlotter.DrawWindowEventHandler;
				dataRecognizer.DataRefined += UpdateLog_DataRefined;
				
				dir.GetPackets();
                WriteOnFile(dataRecognizer.GetDataAnalysisResult(dir.Frequency), FilePath + "WatchMotion-FinalResults.txt");
				WriteOnFile(dir.GetCsvString(), FilePath + FileName + "-" + dir.ClientID + "-" + dir.Frequency + "hz" + FileExt);
				WriteOnFile(dataRecognizer.GetMotionStringBuilder().ToString(), FilePath + "WatchMotion-Logs.txt");
				//WriteOnFile(dataRecognizer.GetLayStandSitStringBuilder().ToString(), FilePath + "laystandsit.txt");
				progress.Report(new ControlsState("Chiusa", Color.Red));
			}
			catch (InvalidPacketException e)
			{
				MessageBox.Show(e.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch(System.IO.IOException ioe)
			{
				if (ioe.HResult == -2146232800) // ID unico dell'eccezione generata da .NET per aver interroto una chiamata bloccante
				{
					MessageBox.Show("Trasferimento annullato.", "Avviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (ioe.HResult == -2147024864)
				{
					MessageBox.Show("Impossibile scrivere il file, controllare che non sia aperto in un altro programma.", "Avviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else throw;
			}
			catch (ConnectionLostException e)
			{
				MessageBox.Show(e.Message);
			}
		}

		/// <summary>
		/// Esegue il sistema.
		/// </summary>
		/// <param name="port">Specifica una porta per l'ascolto.</param>
		/// <param name="progress">Progress per l'aggiornamento della label con lo stato di connessione.</param>
		/// <param name="chartProgress">Progress per l'inserimento dei punti nel grafico.</param>
		/// <param name="chartUpdate">Progress per l'aggiornamento del grafico.</param>
		/// <param name="chartReset">Progress per il reset del grafico.</param>
		public void Start(object port, 
			IProgress<ControlsState> progress, 
			IProgress<ChartPoint> chartProgress,
			IProgress<int> chartUpdate,
			IProgress<int> chartReset,
			IProgress<ChartPointDR> chartProgressDeadReckoning)
		{
			int newPort = (int)port;
			if (this.port != newPort)
			{
				this.port = newPort;
				dir.ChangePort(this.port);
			}
			this.Start(progress, chartProgress, chartUpdate, chartReset, chartProgressDeadReckoning);
		}
		
		/// <summary>
		/// Scrive su file il contenuto al percorso specificato.
		/// </summary>
		/// <param name="content">Stringa contenete i valori da scrivere su file.</param>
		/// <param name="path">Percorso del file.</param>
		public void WriteOnFile(string content, string path)
		{
			System.Security.Permissions.FileIOPermission perm = new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.AllAccess, path);
			perm.Assert();
			using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path, false))
			{
				sw.Write(content);
			}
		}

		/// <summary>
		/// Interrompe ogni operazione che il sistema sta eseguendo.
		/// </summary>
		public void Abort()
		{
			lastLogIndex = 0;
			dir.Abort();
			dir = new Director(port);
			dir.DataReceivedEvent += dir_DataReceivedEvent;
			dir.DifferentNumberOfSensorEvent += dir_DifferentNumberOfSensorEvent;
		}

		void dir_DataReceivedEvent(object sender, EventArgs e)
		{
			MessageBox.Show("Tutti campioni sono stati ricevuti.", "Avviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		void dir_DifferentNumberOfSensorEvent(object sender, EventArgs e)
		{
			progress.Report(new ControlsState("Sensors", Color.Black, s: dir.NumberOfSensors));
		}

		void UpdateLog_DataRefined(object sender, RefinedDataEventArgs args)
		{
			string log = dataRecognizer.GetMotionStringBuilder().ToString().Substring(lastLogIndex);
			lastLogIndex = dataRecognizer.GetMotionStringBuilder().Length;
			progress.Report(new	ControlsState("", Color.Black, l: log));
		}
	}

	public struct ControlsState
	{
		public string text;
		public Color color;
		public int numberOfSensors;
		public string logs;
		public ControlsState(string t, Color c, string l = "", int s = 5)
		{
			text = t;
			color = c;
			numberOfSensors = s;
			logs = l;
		}
	}	
}
