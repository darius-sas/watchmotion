﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using ProgettoCSharp.DataManagement;

namespace ProgettoCSharp
{
	namespace DataAnalysis
	{
		public delegate void DataAnalized(object sender, RefinedDataEventArgs e);
		public class Recognizer : AbstractRecognizer
		{
			DateTime startTime;
			int seconds;
			float lastThetaVal;
			DiscontinuityHolder deltaGirata; // Memorizza il cambiamento di segno tra una finestra e l'altra
			DiscontinuityHolder deltaYaw;	 // Da usare su FunzioneOrientamento e passarglielo come parametro
			float v0;						 // Velocità iniziale a ogni inizio finestra.
			PointF x0;

			public Recognizer()
			{
				lastThetaVal = float.MinValue;
				seconds = 0;
				deltaGirata = new DiscontinuityHolder();
				deltaYaw = new DiscontinuityHolder();
				startTime = DateTime.Now;
				v0 = 0f;
				x0 = new PointF(0f, 0f);
			}

			/// <summary>
			/// Esegue i calcoli per estrapolare le informazioni dai sensori.
			/// </summary>
			sealed protected override void Recognize()
			{
				// Moto
				motionStringBuilder.Append("Secondo:" + seconds++ + Environment.NewLine);
				for (int i = 0; i < sensors.Length; i++)
				{
					sensors[i].IsMoving();
					motionStringBuilder.Append(sensors[i].ToString());
				}				
				// Girata
				currentWindowData.theta = sensors[0].FunzioneOrientamento();
				float[] smoothTheta = Analysis.Smoothing(Analysis.ExtractSensorData(currentWindowData.theta, 1), 2);
				for (int i = 0; i < currentWindowData.theta.GetLength(0); i++)
				{
					currentWindowData.theta[i, 1] = smoothTheta[i];
				}
				CalculateTurnAngle(smoothTheta); // Calcola l'angolo di girata sul
				// LayStandSit
				positionState = IsLayStandSit(currentWindowData.sampleWin);
				layStandSitBuilder.Append(positionState.ToString() + "\n");
				motionStringBuilder.AppendFormat("Turn: {0}° | ", TurnAngle);
				motionStringBuilder.Append(positionState);
				motionStringBuilder.Append(Environment.NewLine + Environment.NewLine);
			}

			/// <summary>
			/// Cerca l'angolo, in radianti e imposta la variabile turnAngleRadiants.
			/// </summary>
			/// <param name="theta">Vettore contente i gli angoli a ogni tempo t.</param>
			sealed protected override void CalculateTurnAngle(float[] theta)
			{
				if (lastThetaVal == float.MinValue)
				{
					turnAngleRadiants = theta[theta.Length - 1] - theta[0];
				}
				else
				{
					turnAngleRadiants = theta[theta.Length - 1] - lastThetaVal;
				}
				lastThetaVal = theta[theta.Length - 1];
			}

			/// <summary>
			/// Riconosce se una persona è seduta, sdraiata o in piedi.
			/// </summary>
			/// <param name="mat">Matrice dei campioni.</param>
			/// <returns>Lo stato di una persona: seduta, sdraiata o in piedi.</returns>
			sealed protected override PositionState IsLayStandSit(float[, ,] mat)
			{
				float[] fstddev = new float[mat.GetLength(1)];
				for (int i = 0; i < mat.GetLength(1); i++)
				{
					fstddev[i] = mat[1, i, 0];
				}
				fstddev = Analysis.FloatingStdDev(fstddev, 10);
				int start = 0, length = 0, maxLength = length, end = 0;
				for (int i = 0; i < fstddev.Length; i++) // Calcola l'intervallo massimo
				{
					if (fstddev[i] < Thresholds.ALPHA_LAYSTANDSIT)
					{
						if (length == 0)
						{
							start = i;
						}
						length++;
					}
					else
					{
						if (maxLength < length)
						{
							maxLength = length;
							end = start + length;
						}
						length = 0;
						start = i;
					}
				}
				start = end - maxLength;
				int t = (new Random()).Next(start, end);
				float y = mat[1, t, 0];
				return Analysis.LayStandSit(y);
			}

			/// <summary>
			/// Restituisce un sommario in formato leggibile all'utente delle varie informazioni ricavate dall'analisi.
			/// </summary>
			public override String GetDataAnalysisResult(int frequency)
			{
				double positionStart = 0, positionDurationLength = 0;
                double add = (1.0d / (double)frequency);
				String s = "";
				PositionState positionState = actionList[0].state;
				for (int time = 0; time < actionList.Count; time++)
				{
					if (time < actionList.Count - 1)
					{
						if (actionList[time].state != actionList[time + 1].state)
						{
                            positionDurationLength = (time + 1 )* add;
							s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, positionDurationLength, positionState);
							positionStart = positionDurationLength;
							positionState = actionList[time + 1].state;
						}
					}
					else
					{
                        positionDurationLength = (time + 1) * add;
						if (actionList[time].state == positionState)
							s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, positionDurationLength, positionState);
						else
						{
							s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, (positionDurationLength - add), positionState);
							positionState = actionList[time].state;
                            s = s + String.Format("Da {0} a {1} : {2}\r\n", (positionDurationLength - add), positionDurationLength, positionState);
						}
					}
				}
				s = s + "\r\n";
				positionStart = 0;

				for (int time = 0; time < actionList.Count; time++)
				{
					if (time < actionList.Count - 1)
					{
						if (actionList[time].actions[0].motion != actionList[time + 1].actions[0].motion)
						{
                            positionDurationLength = (time + 1) * add;
							if (actionList[time].actions[0].motion)
								s = s + String.Format("Da {0} a {1} : Non-fermo\r\n", positionStart, positionDurationLength);
							else
								s = s + String.Format("Da {0} a {1} : Fermo\r\n", positionStart, positionDurationLength);
							positionStart = positionDurationLength;
						}
					}
					else
					{
                        positionDurationLength = (time + 1) * add;
						if (actionList[time].actions[0].motion == actionList[time - 1].actions[0].motion)
						{
							if (actionList[time].actions[0].motion)
								s = s + String.Format("Da {0} a {1} : Non-fermo\r\n", positionStart, positionDurationLength);
							else
								s = s + String.Format("Da {0} a {1} : Fermo\r\n", positionStart, positionDurationLength);
						}
						else
						{
							if (actionList[time].actions[0].motion)
								s = s + String.Format("Da {0} a {1} : Non-fermo\r\n", positionDurationLength - add, positionDurationLength);
							else
								s = s + String.Format("Da {0} a {1} : Fermo\r\n", positionDurationLength - add, positionDurationLength);
						}
					}
				}

				s = s + "\r\n";
				positionStart = 0;
				String state = "dritto";
				int angle = 0;
				for (int time = 0; time < actionList.Count; time++)
				{
					if (Math.Abs(actionList[time].turnAngleDegrees) >= 10)
					{
						if (Math.Sign(actionList[time].turnAngleDegrees) > 0)
						{
							if (state != "girata-sx")
							{
                                positionDurationLength = (time + 1) * add;
								if (state == "dritto")
									s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, positionDurationLength, state);
								else
									s = s + String.Format("Da {0} a {1} : {2} {3} GRADI\r\n", positionStart, positionDurationLength, state, angle);

								positionStart = positionDurationLength;
								state = "girata-sx";
								angle = actionList[time].turnAngleDegrees;
							}
							else
								angle = actionList[time].turnAngleDegrees;
						}
						else
						{
							if (state != "girata-dx")
							{
                                positionDurationLength = (time + 1) * add;
								if (state == "dritto")
									s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, positionDurationLength, state);
								else
									s = s + String.Format("Da {0} a {1} : {2} {3} GRADI\r\n", positionStart, positionDurationLength, state, angle);

								positionStart = positionDurationLength;
								state = "girata-dx";
								angle = actionList[time].turnAngleDegrees;
							}
							else
								angle = actionList[time].turnAngleDegrees;
						}
					}
					else
					{
						if (state != "dritto")
						{
                            positionDurationLength = (time + 1) * add;
							s = s + String.Format("Da {0} a {1} : {2} {3} GRADI\r\n", positionStart, positionDurationLength, state, angle);
							positionStart = positionDurationLength;
							state = "dritto";
							angle = 0;
						}
					}
				}

				positionDurationLength = actionList.Count * add;
				if (state != "dritto")
					s = s + String.Format("Da {0} a {1} : {2} {3} GRADI\r\n", positionStart, positionDurationLength, state, angle);
				else
					s = s + String.Format("Da {0} a {1} : {2}\r\n", positionStart, positionDurationLength, state);
				return s;
			}

			/// <summary>
			/// Calcola il percorso fatto dal soggetto nella finestra corrente.
			/// </summary>
			/// <param name="frequency">Frequenza di campionamento.</param>
			sealed protected override void DeadReckoning(int frequency)
			{
				float[] a = Analysis.Smoothing(Analysis.ExtractArray(currentWindowData.sampleWin, 1, 0), 5);
				float[] yaws = GetWindowYaws();
				if (positionState == PositionState.STAND && (sensors[3].IsMoving() || sensors[4].IsMoving()))
				{
					int	subWindows = (int) (currentWindowData.sampleWin.GetLength(1) * 0.1f);
					float mean, s, acc = 0;			
					float time = (float)(currentWindowData.sampleWin.GetLength(1)) / (float)frequency;
					PointF[] x = new PointF[subWindows];

					for (int i = 0, j = 0; i < x.Length; i++, j+=10)
					{
						if (!IsAccelerationConst(a, out mean, j, j + 9))
							if (a[j] < a[j + 9])
								acc = -(a[j] + a[j + 9]) / 2;
							else
								acc = -(a[j] + a[j + 9]) / 2 * 0.6f;
						else
							acc = (Math.Abs(mean) < 1.5f) ? 0 : -mean;

						s = (float)(v0 * time + (acc * Math.Pow(time, 2)) / 2);
						x[i] = (i == 0) ? new PointF(x0.X, x0.Y) : new PointF(x[i - 1].X, x[i - 1].Y);
						x[i].X += (float)Math.Cos(yaws[j]) * (s / subWindows);
						x[i].Y += (float)Math.Sin(yaws[j]) * (s / subWindows);
					}
					currentWindowData.deadReckoningMotion = x;
					v0 = v0 * 0.1f + acc * time;
					x0 = x[x.Length-1];
				}
				else
				{
					v0 = 0;
					currentWindowData.deadReckoningMotion = null;
				}
				for (int i = 0; i < yaws.Length; i++)
				{
					currentWindowData.theta[i, 2] = yaws[i];
					currentWindowData.theta[i, 3] = a[i];
				}
			}

			/// <summary>
			/// Calcola se i valori dell'accelerazione sono costanti nell'intervallo [start, end].
			/// </summary>
			/// <param name="acceleration">Array con i valori dell'accelerazione.</param>
			/// <param name="mean">out. Verrà salvata la media dei valori tra [start, end].</param>
			/// <param name="start">Indice di inizio.</param>
			/// <param name="end">Indice di fine.</param>
			/// <returns>True se l'accelerazione è costante in [start, end], false altrimenti.</returns>
			private bool IsAccelerationConst(float[] acceleration, out float mean, int start, int end)
			{
				if (start < 0 || start > end || end > acceleration.Length)
					throw new ArgumentOutOfRangeException("I parametri 'start' e/o 'end' hanno valori non validi.");
				int k = 0;
				mean = Analysis.Mean(acceleration, start, end);
				float stdDev = Analysis.StdDev(acceleration, start, end);
				float inf = mean - stdDev, sup = mean + stdDev;
				
				for (int i = start; i < end; i++)
					if (inf <= acceleration[i] && acceleration[i] <= sup)
						k++;

				return k >= (acceleration.Length * 0.8f);
			}

			/// <summary>
			/// Calcola i valori di Yaw in ogni momento della finestra ed elimina le discontinuità.
			/// </summary>
			/// <returns>Valori di Yaw in ogni momento t senza discontinuità.</returns>
			private float[] GetWindowYaws()
			{
				float q0, q1, q2, q3;
				float[] yaws = new float[currentWindowData.sampleWin.GetLength(1)];
				for (int i = 0; i < currentWindowData.sampleWin.GetLength(1); i++)
				{
					q0 = currentWindowData.sampleWin[9, i, 0];
					q1 = currentWindowData.sampleWin[10, i, 0];
					q2 = currentWindowData.sampleWin[11, i, 0];
					q3 = currentWindowData.sampleWin[12, i, 0];
					yaws[i] = Analysis.Yaw(q0, q1, q2, q3);
				}
				return Analysis.Smoothing(Analysis.RemoveAtanDiscontinuity(yaws, deltaYaw), 3);
			}
		}

		public class Sensor
		{
			public int SensorNumber { get; private set; }
			/// <summary>
			/// E' l'angolo di inclinazione tra l'asse x e l'asse z.
			/// </summary>
			public int MotionDegrees { get { return (int)(motionDirection * 180 / Math.PI); } }
			/// <summary>
			/// L'orientamento rispetto al polo nord.
			/// </summary>
			public int OrientationDegrees { get { return (int)(orientationDirection * 180 / Math.PI); } }
			float modAccStdDev;
			float motionDirection;
			bool? isMoving;
			float orientationDirection;
			RefinedDataEventArgs data;

			DiscontinuityHolder delta; // Memorizza il cambiamento di segno tra una finestra e l'altra

			public Sensor(int sensor, RefinedDataEventArgs data)
			{
				this.SensorNumber = sensor;
				this.data = data;
				this.motionDirection = 0f;
				this.orientationDirection = 0f;
				this.isMoving = null;
				this.delta = new DiscontinuityHolder();
			}

			/// <summary>
			/// Calcola i moduli dell'accelerometro, del giroscopio
			/// </summary>
			public void CalculateModules()
			{
				for (int t = 0; t < data.modacc.GetLength(0); t++)
				{
					data.modacc[t, SensorNumber] = Analysis.Module(data.sampleWin[0, t, SensorNumber],
						data.sampleWin[1, t, SensorNumber],
						data.sampleWin[2, t, SensorNumber]);
					data.modgyr[t, SensorNumber] = Analysis.Module(data.sampleWin[3, t, SensorNumber],
						data.sampleWin[4, t, SensorNumber],
						data.sampleWin[5, t, SensorNumber]);
					data.modmag[t, SensorNumber] = Analysis.Module(data.sampleWin[6, t, SensorNumber],
						data.sampleWin[7, t, SensorNumber],
						data.sampleWin[8, t, SensorNumber]);
				}
			}

			/// <summary>
			/// Calcola i gradi di girata rispetto alla posizione nella finestra precedente.
			/// </summary>
			/// <returns>Una matrice contenente sulla prima colonna i valori di theta con discontinuità
			///			 e sulla seconda quelli senza.</returns>
			public float[,] FunzioneOrientamento()
			{
				float[] magY = Analysis.ExtractArray(data.sampleWin, 7, SensorNumber);
				float[] magZ = Analysis.ExtractArray(data.sampleWin, 8, SensorNumber);

				float[] tmp = new float[magY.Length];

				for (int i = 0; i < magY.Length; i++)
				{
					tmp[i] = (float)Math.Atan(magY[i] / magZ[i]);
				}
				float[,] theta = new float[tmp.Length, 5];
				for (int i = 0; i < tmp.Length; i++)
				{
					theta[i, 0] = tmp[i];
					theta[i, 1] = tmp[i] + Analysis.sign;
					if (Math.Abs(tmp[i]) >= Thresholds.DISCONTINUITY && i < tmp.Length - 1)
					{
						if (Math.Sign(tmp[i]) != Math.Sign(tmp[i + 1]))
						{
							if (tmp[i] > tmp[i + 1])
							{
								Analysis.sign = Analysis.sign + (float)Math.PI;
							}
							else
							{
								Analysis.sign = Analysis.sign - (float)Math.PI;
							}
						}
					}
				}
				var smooth = Analysis.Smoothing(Analysis.ExtractSensorData(theta, 1), 5);
				for (int i = 0; i < theta.GetLength(0); i++)
				{
					theta[i, 1] = smooth[i];
				}
				return theta;
			}

			/// <summary>
			/// Controlla la matrice del modulo accelerazione per riconoscere il moto.
			/// </summary>
			/// <returns>True se il sensore è in movimento. False altrimenti.</returns>
			public bool IsMoving()
			{
				if (isMoving != null)
					return (bool)isMoving;
				isMoving = false;				
				float[] sensorModacc = new float[data.modacc.GetLength(0)];
				float[] sensorSmoothModacc = new float[data.modacc.GetLength(0)];

				sensorModacc = Analysis.ExtractSensorData(data.modacc, SensorNumber);
				sensorSmoothModacc = Analysis.Smoothing(sensorModacc, 5);
				modAccStdDev = Analysis.StdDev(sensorSmoothModacc);
				if (modAccStdDev > Thresholds.MIN_STDDEV_MOTION)
				{
					motionDirection = CalculateMotionDirection(sensorSmoothModacc);
					isMoving = true;
				}
				return modAccStdDev > Thresholds.MIN_STDDEV_MOTION;
			}

			/// <summary>
			/// Cerca nel modulo dell'accelerazione smorzato(smoothing) un valore della deviazione standard massimo
			/// e calcola la direzione del vettore accelerazione in quel punto.
			/// </summary>
			/// <param name="smoothModAcc"></param>
			private float CalculateMotionDirection(float[] smoothModAcc)
			{
				/* Se la deviazione standard tende a zero i valori si assestano vicino alla media.
				 * Se la deviazione standard cresce o è molto maggiore di 0 allora 
				 * i valori si discostano molto dalla media. 
				 * Si potrebbe usare questo per capire quando una persona cambia di stato.
				 * Oppure per decidere quale Tempo usare per scegliere l'orientamento del sensore.
				 */
				int indexOfMax = Analysis.IndexOfMax_Abs(smoothModAcc);
				return (float)Math.Atan2(data.sampleWin[0, indexOfMax, SensorNumber],
				   data.sampleWin[2, indexOfMax, SensorNumber]);
			}

			/// <summary>
			/// Salva i dati del sensore sottoforma di Action.
			/// </summary>
			/// <returns>Un oggetto Action contenente i risultati dei dati analizzati.</returns>
			public Action GetAction()
			{
				Action action = new Action(MotionDegrees, (bool)isMoving, OrientationDegrees);
				return action;
			}

			/// <summary>
			/// Restituisce una rappresentazione a stringa dello stato del sensore.
			/// </summary>
			/// <returns>Una stringa contenete i risultati con timestamp dell'ore della rilevazione.</returns>
			public override string ToString()
			{
				return (bool)isMoving ? String.Format("S {0}: moto, t: {1}, θ: {2}.\n", SensorNumber + 1, DateTime.Now.ToString("HH:mm:ss tt"), MotionDegrees) :
					String.Format("S {0}: fermo, t: {1}.\n", SensorNumber + 1, DateTime.Now.ToString("HH:mm:ss tt"));
			}
		}

		public struct ActionListEntry
		{
			public Action[] actions;
			public PositionState state;
			public int turnAngleDegrees;
			public DateTime timestamp;
			//public float acceleration;
			//public float gyroTurnAngleDegrees;

			public ActionListEntry(Action[] actions, PositionState state, int turnAngleDegrees)
			{
				this.actions = actions;
				this.state = state;
				this.turnAngleDegrees = turnAngleDegrees;
				timestamp = DateTime.Now;
			}
		}

		public class Action
		{
			public int motionDegrees;
			public bool motion;
			public int orientationDegrees;

			public Action(int motionDegrees, bool motion, int orientationDegrees)
			{
				this.motionDegrees = motionDegrees;
				this.motion = motion;
				this.orientationDegrees = orientationDegrees;
			}
		}

		public class RefinedDataEventArgs : EventArgs
		{
			public float[, ,] sampleWin;
			public float[,] modacc;
			public float[,] modgyr;
			public float[,] modmag;
			public float[,] theta;
			public PointF[] deadReckoningMotion;

			public RefinedDataEventArgs(float[, ,] sampW, float[,] macc, float[,] mgyr, float[,] mmag, float[,] atan)
			{
				sampleWin = sampW;
				modacc = macc;
				modgyr = mgyr;
				modmag = mmag;
				theta = atan;
			}
		}
	}
}