﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProgettoCSharp;

namespace ProgettoCSharp
{
	namespace DataManagement
	{
		public delegate void DifferentNumberOfSensor(object sender, EventArgs e);
		public delegate void DataReceived(object sender, EventArgs e);
		public class Director
		{
			Receiver packetReceiver;
			SampleBuilder builder;
			public event DataReceived DataReceivedEvent;
			public event DifferentNumberOfSensor DifferentNumberOfSensorEvent;
			public string ClientID { get; private set; }
			public int Frequency { get; private set; }
			public int BytesReceived { get { return packetsReceived * 268; } }
			public int NumberOfSensors { get; private set; }
			private int packetsReceived;
			
			/// <summary>
			/// Fare più costruttori per i vari builder o con quello di default.
			/// </summary>
			/// <param name="port"></param>
			public Director(int port)
			{
				packetReceiver = new Receiver(port);
				ClientID = "";
				Frequency = -1;
			}

			/// <summary>
			/// Si connette al simulatore e riceve il pacchetto di handshake.
			/// </summary>
			/// <returns>True se la connessione è andata a buon fine. False altrimenti.</returns>
			public bool SetUpConnection()
			{
				byte[] connectionPacket = packetReceiver.Listen();
				if (connectionPacket == null)
				{
					return false;
				}
				ClientID = Encoding.UTF8.GetString(connectionPacket, 0, 10);
				Frequency = (int)connectionPacket[10];
				builder = new SampleBuilder(Frequency);
				return true;
			}

			/// <summary>
			/// Riceve tutti i pacchetti che il simulatore invia e costruisce l'oggetto SampleWindow utilizzando un ISampleBuilder.
			/// </summary>
			/// <exception cref="InvalidPacketException">Sollevata quando un pacchetto non è valido.</exception>
			public void GetPackets()
			{
				byte[] packet = packetReceiver.ReceiveSamples();
				packetsReceived++;				
				if (packetReceiver.NumberOfSensors != 5) // In teoria si potrebbe anche non fare questo controllo ed eseguire comunque il metodo.
				{
					NumberOfSensors = packetReceiver.NumberOfSensors;
					builder.UpdateNumberOfSensors(NumberOfSensors);
					DifferentNumberOfSensorEvent(this, null);
				}
				builder.BuildSample(packet);
				while (true)
				{
					if (packet[0] != 0xFF)
					{
						throw new InvalidPacketException("Un pacchetto è stato ricevuto in modo scorretto.");
					}
					packet = packetReceiver.ReceiveSamples();
					if (packet.Length == 0)
					{
						GetBuilder().ClearWindow();
						DataReceivedEvent(this, new EventArgs());
						break;
					}
					builder.BuildSample(packet);
					packetsReceived++;
				}
				packetReceiver.Close();
			}

			/// <summary>
			/// Chiude la connessione.
			/// </summary>
			public void Abort()
			{
				packetReceiver.Close();
			}

			/// <summary>
			/// Richiede la stringa in formato .csv contenente tutti i dati ricevuti.
			/// </summary>
			/// <returns>La stringa in formato .csv con i dati grezzi.</returns>
			public string GetCsvString()
			{
				return builder.GetCsvString();
			}

			/// <summary>
			/// Cambia la porta su cui ascoltare le richieste di connessione.
			/// </summary>
			/// <param name="port">La nuova porta.</param>
			public void ChangePort(int port)
			{
				packetReceiver = new Receiver(port);
			}

			/// <summary>
			/// Ritorna l'istanza del builder utilizzata per costruire una finestra SampleWindow.
			/// </summary>
			/// <returns>L'istanza del builder.</returns>
			public SampleBuilder GetBuilder()
			{
				return builder;
			}
		}
	}
}
