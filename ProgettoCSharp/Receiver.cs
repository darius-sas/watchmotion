﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace ProgettoCSharp
{
	namespace DataManagement
	{
		public struct Constants
		{
			public const int CONNECTION_PACKET_LENGTH	= 14;
			public const int FIVE_SENSORS_PACKET_LENGTH = 268;
			public const int FIVE_SENSORS_DATA_LENGTH	= 260;
			public const int TOOLS_NUMBER				= 13;
            public const int SAMPLE_WINDOW_LENGTH		= 1;
		}
		
		public class Receiver
		{
			TcpListener server;
			TcpClient sender;
			BinaryReader bin;
			bool isSetNumberOfPackets;
			int packetsLength;
			public int NumberOfSensors
			{
				get { return packetsLength < 255 ? (packetsLength - 6) / 52 : (Constants.FIVE_SENSORS_DATA_LENGTH) / 52; }
			}

			public Receiver(int port)
			{
				server = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
				sender = null;
				bin = null;
				isSetNumberOfPackets = false;
				packetsLength = 0;
			}
	
			/// <summary>
			/// Ascolta sulla porta definita durante la creazione e riceve il pacchetto di connessione.
			/// </summary>
			/// <returns>Il pacchetto di connessione (handshake).</returns>
			public byte[] Listen()
			{
				server.Start();
				try
				{
					sender = server.AcceptTcpClient();
				}
				catch (SocketException sExc)
				{
					if (sExc.ErrorCode == 10004) // L'esecuzione è stata annullata dall'utente.
					{
						return null;
					}
					else
					{
						throw;
					}
				} 
				server.Stop();
				byte[] connectionPacket = new byte[Constants.CONNECTION_PACKET_LENGTH];
				bin = new BinaryReader(sender.GetStream());
				return bin.ReadBytes(Constants.CONNECTION_PACKET_LENGTH);
			}
		
			/// <summary>
			/// Riceve un pacchetto. Appena viene ricevuto il primo pacchetto analizza la lunghezza del campo data
			/// e calcola il numero di sensori memorizzandolo in NumberOfSensors.
			/// </summary>
			/// <returns>Un pacchetto ricevuto.</returns>
			public byte[] ReceiveSamples()
			{
				if (!isSetNumberOfPackets)
				{
					byte[] overhead = bin.ReadBytes(3);
					if (overhead.Length == 0)
					{
						throw new ConnectionLostException("Connessione persa.");
					}
					if (overhead[2] == 255)
					{
						packetsLength = Constants.FIVE_SENSORS_PACKET_LENGTH;
					}
					else
					{
						packetsLength = overhead[2] + 6;
					}
					isSetNumberOfPackets = true;
					byte[] restOfThePacket = bin.ReadBytes(packetsLength - 3);
					byte[] firstPacket = new byte[packetsLength];

					for (int i = 0; i < packetsLength; i++)
					{
						if (i < overhead.Length)
						{
							firstPacket[i] = overhead[i];
						}
						else
						{
							firstPacket[i] = restOfThePacket[i - overhead.Length];
						}
					}
					return firstPacket;
				}
				return bin.ReadBytes(packetsLength);
			}

			/// <summary>
			/// Chiude la connessione.
			/// </summary>
			public void Close()
			{
				if (bin != null && sender != null)
				{
					bin.Close();
					sender.Close();
				}				
				server.Stop();
			}
		}
	}
}
