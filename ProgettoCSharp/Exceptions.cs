﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCSharp
{
	namespace DataManagement
	{
		[Serializable]
		public class InvalidPacketException : Exception
		{
			public InvalidPacketException() { }
			public InvalidPacketException(string message) : base(message) { }
			public InvalidPacketException(string message, Exception inner) : base(message, inner) { }
			protected InvalidPacketException(
			  System.Runtime.Serialization.SerializationInfo info,
			  System.Runtime.Serialization.StreamingContext context)
				: base(info, context) { }
		}

		[Serializable]
		public class PacketsReceivedException : Exception
		{
			public PacketsReceivedException() { }
			public PacketsReceivedException(string message) : base(message) { }
			public PacketsReceivedException(string message, Exception inner) : base(message, inner) { }
			protected PacketsReceivedException(
			  System.Runtime.Serialization.SerializationInfo info,
			  System.Runtime.Serialization.StreamingContext context)
				: base(info, context) { }
		}
		[Serializable]
		public class ConnectionLostException : Exception
		{
			public ConnectionLostException() { }
			public ConnectionLostException(string message) : base(message) { }
			public ConnectionLostException(string message, Exception inner) : base(message, inner) { }
			protected ConnectionLostException(
			  System.Runtime.Serialization.SerializationInfo info,
			  System.Runtime.Serialization.StreamingContext context)
				: base(info, context) { }
		}
	}
}
